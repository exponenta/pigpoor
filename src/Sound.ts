namespace PigPoorGame {
    export interface ISound {
        name:string;
        src:string;
        preload?: boolean;
    }
    
    export class SoundManager {

        sounds:{[id:string]: Howl} = {};

        constructor(manifest:ISound[]) {

            for (const etry  of manifest) {
                
                const sound = new Howl({
                    src: etry.src,
                    preload: etry.preload,
                });

                this.sounds[etry.name] = sound;
            }
        }

        Play(name:string, loop: boolean = false, volume: number = 1, fadeIn: boolean = false): Howl {
            const s = this.sounds[name];
            if(s){
                s.loop(loop);
                s.play();
                if(fadeIn){
                    s.fade(0, volume, 1);
                }else {
                    
                    s.volume(volume);
                }
            }
            return s;
        }
        
        Stop(name: string, fadeout: boolean = false) {
            if(this.sounds[name]){
                
                if(!fadeout){
                    this.sounds[name].stop();
                }else {
                    const v = this.sounds[name].volume();
                    this.sounds[name].fade(v, 0, 1);
                    setTimeout(()=>{
                        this.sounds[name].stop();
                    }, 500);
                }
            }
        }

    }
}