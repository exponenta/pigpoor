namespace PigPoorGame {
	export class ObjectPool<T> {
		usedArr: Array<T> | undefined = [];
		freeArr: Array<T> | undefined = [];

		isFixed: boolean = false;

		buildFunc: () => T;
		preFunc: (obj: T) => void;
		postFunc: (obj: T) => void;

		constructor(buildFunc: () => T, preFunc: (obj: T) => void, postFunc: (obj: T) => void) {
			this.buildFunc = buildFunc;
			this.preFunc = preFunc;
			this.postFunc = postFunc;
		}

		populate(count: number = 20, fixed: boolean = false) {
			this.isFixed = fixed;
			this.freeArr = [];
			for (var i = 0; i < count; i++) {
				this.freeArr.push(this.buildFunc());
			}
		}

		clear(): void {
			this.freeArr = undefined;
			this.usedArr = undefined;
		}

		get total(): number {
			if (this.freeArr && this.usedArr) return this.freeArr.length + this.usedArr.length;

			return 0;
		}

		get free(): number {
			if (!this.freeArr) return 0;

			return this.freeArr.length;
		}

		pop(): T | undefined {
			const obj = this.freeArr!.pop();

			if (obj) {
				if (!this.usedArr) this.usedArr = [];
				this.usedArr.push(obj);

				this.preFunc(obj);
				return obj;
			} 

			if(this.isFixed) {
				if(this.usedArr!.length == 0){
					return undefined;
				}

				const last = this.usedArr!.splice(0,1)![0];
				this.postFunc(last);
				this.preFunc(last);
				this.usedArr!.push(last);
				return last;
			}

			this.populate(1);
			return this.pop();
		}

		push(obj: T): boolean {
			if (!obj || !this.usedArr) return false;

			const index = this.usedArr.indexOf(obj);
			if (this.usedArr && index > -1) {
				this.postFunc(obj);
				this.usedArr.splice(index, 1);
				this.freeArr!.push(obj);

				return true;
			}
			return false;
		}

		pushAll(): boolean {
			
			if(!this.usedArr || !this.freeArr) return false;

			for(let  i = this.usedArr.length - 1; i >= 0;  i--) {
				this.push(this.usedArr[i]);
			}
			return true;
		}
	}
}
