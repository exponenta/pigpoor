namespace PigPoorGame {
    export class PigPoolElement extends PIXI.Container {
		zScale: number = 1;
		tint: number = 0xffffff;
		speed: number = 140;
		sprite: PIXI.extras.AnimatedSprite;
		defScale: number = 1;
		direction: number = 0;
		isUsed: boolean = false;

		constructor(animatedSprite: PIXI.extras.AnimatedSprite) {
			super();
			this.sprite = animatedSprite;
			this.defScale = this.sprite.scale.x;
			this.addChild(animatedSprite);
		}

		update(dt: number) {
			this.position.x += (dt * this.direction * this.speed * this.zScale) / 60;
			this.scale.set(this.direction * this.defScale * this.zScale, this.defScale * this.zScale);
			this.sprite.tint = this.tint;
		}
	}

}