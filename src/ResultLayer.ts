
namespace PigPoorGame {
	export class ResultLayer implements ILayer {
		stage?: TiledOG.TiledContainer | undefined;
		app: App;
		wasInited: boolean = false;

		ratingHtml: HTMLElement;
		ratingContainer?: PIXI.Container;

		private totalScoreText?: PIXI.Text;

		constructor(app: App) {
			this.app = app;
			this.ratingHtml = document.querySelector("#result-table")! as HTMLElement;
		}

		addToLoader(loader: PIXI.loaders.Loader, callback?: (() => void) | undefined): PIXI.loaders.Loader {
			loader.add("result", "./maps/result.json", () => {
				if (callback) callback();
				this.stage = loader.resources["result"].stage;
			});

			return loader;
		}

		init(): void {
			const preset = {
				hover: 0xc2adc6,
				disabled: 0x5b61b3,
				click: 0x865e84
			};

			if (this.wasInited) return;

			const playAgainButton = this.stage!.getChildByPath<PIXI.Sprite>("Result/PlayAgainButton");
			Utils.registerAsButton(playAgainButton!, preset, () => {
				this.app.activateLayer(this.app.layers["game"]);
			});

			const playAgainButtonRaiting = this.stage!.getChildByPath<PIXI.Sprite>("Board/PlayAgainButton");
			Utils.registerAsButton(playAgainButtonRaiting!, preset, () => {
				this.app.activateLayer(this.app.layers["game"]);
			});

			const ratingButton = this.stage!.getChildByPath<PIXI.Sprite>("Result/RatingButton");

			Utils.registerAsButton(ratingButton!, preset, () => {
				this.showResultBoard();
			});

			this.totalScoreText = this.stage!.getChildByPath<TiledOG.TiledContainer>("Result/TotalScore")!.text;
			this.updateText(0);

			this.ratingContainer = this.stage!.getChildByPath<TiledOG.TiledContainer>("Board/RaitingBox");
			this.ratingContainer!.visible = false;

			this.wasInited = true;

			window.addEventListener("resize", () => {
				Utils.mapTransformToHTML(this.ratingContainer!, this.ratingHtml!, this.app.renderer, {
					preferTextSize: true,
					targetTextSize: 22
				});
			});
		}

		showResultBoard(): void {
			this.stage!.getChildByPath<TiledOG.TiledContainer>("Result")!.visible = false;
			this.stage!.getChildByPath<TiledOG.TiledContainer>("Board")!.visible = true;

			Utils.mapTransformToHTML(this.ratingContainer!, this.ratingHtml!, this.app.renderer, {
				preferTextSize: true,
				targetTextSize: 22
			});

			this.ratingHtml!.style.display = "block";
			//this.ratingHtml!.scrollTo(0, 1);
		}

		hideReslutBoard(): void {
			this.stage!.getChildByPath<TiledOG.TiledContainer>("Result")!.visible = true;
			this.stage!.getChildByPath<TiledOG.TiledContainer>("Board")!.visible = false;

			this.ratingHtml!.style.display = "none";
		}

		fadeProgress(p: number, showing: boolean): void {
			if (!showing) return;
			const parallax: ParallaxLayer = this.app.layers["parallax"] as any;
			parallax!.fader = 1 - p;
		}

		beforeShowed(): void {

			const menI = this.stage!.getChildByPath<PIXI.Sprite>("Result/PlayerM")!;
			const womenI = this.stage!.getChildByPath<PIXI.Sprite>("Result/PlayerW")!;

			menI.visible = this.app.userConfig.gender == UserGender.BOY;
			womenI.visible = this.app.userConfig.gender == UserGender.GIRL;
			
			
			if (this.app.userConfig.score > 0) {
				this.app.netManager.sendScore(this.app.userConfig, resp => {
					this.populateBoard(resp);
				});
			} else {
				this.app.netManager.getScores(resp => {
					this.populateBoard(resp);
				});
			}

			const ga = this.app.gameConfig.greetings;
			if (ga && ga.length > 0) {
				var text = ga[(Math.random() * ga.length) >> 0];
				const view = this.stage!.getChildByPath<TiledOG.TiledContainer>("Result/GreetingsText")!.text;
				
				view!.text = this.app.userConfig.formatText(text);
			}
		}

		populateBoard(resp: UserProfile[] | undefined): void {
			
			this.ratingHtml.innerHTML = "";
			let index = 1;

			if (!resp || (resp && resp.length == 0)) {
				resp = [this.app.userConfig];
			}

			const count = Math.max(5, resp.length);

			for (let i = 0; i < count; i++) {
				const u = resp[i];

				if (u) {
					this.ratingHtml.appendChild(this.genBlock(i + 1, u.name, u.score));
				} else {
					this.ratingHtml.appendChild(document.createElement("div"));
				}
			}

			if(this.ratingHtml!.scrollTo) this.ratingHtml!.scrollTo(0,1);
		}

		genBlock(id: number, name: string, score: number): HTMLElement {
			const node = document.createElement("div");
			node.innerHTML = `<span id="score-id">${id}</span> <span id="score-name">  ${name}</span> <span id="score-text">${score}</span>`;
			return node;
		}

		showed(): void {
			let proxy = {
				val: 0
			};

			this.app.soundManager.Play("counter");
			gsap.TweenLite.to(proxy, 1, {
				val: this.app.userConfig.score,
				onUpdate: () => {
					this.updateText(proxy.val >> 0);
				}
			}).play();
		}

		updateText(score: number) {
			this.totalScoreText!.text = Utils.StringPad(score, 4);
		}

		hidden(): void {
			this.hideReslutBoard();
		}

		update(delta: number): void {}
	}
}
