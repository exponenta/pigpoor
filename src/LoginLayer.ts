
namespace PigPoorGame {
	export class LoginLayer implements ILayer {
		stage?: TiledOG.TiledContainer | undefined;
		app: App;

		private editContainer?: PIXI.DisplayObject;
		private editHtml?: HTMLInputElement;

		private rulleContainer?: TiledOG.TiledContainer;
		private loginContainer?: TiledOG.TiledContainer;

		private selectTitle?: PIXI.DisplayObject;

		constructor(app: App) {
			this.app = app;
			
		}

		addToLoader(loader: PIXI.loaders.Loader, callback?: (() => void)): PIXI.loaders.Loader {
			
			loader.add("login", "./maps/login.json", () => {
				this.stage = loader.resources["login"].stage;
			});
			
			return loader;
		}

		init(): void {

			if (!this.stage) return;

			this.loginContainer = this.stage.getChildByName("Login") as TiledOG.TiledContainer;
			this.rulleContainer = this.stage.getChildByName("Rulles") as TiledOG.TiledContainer;
			this.selectTitle = this.loginContainer.getChildByName("SelectPers");
			this.editContainer = this.loginContainer.getChildByName("EditEnterName");
			this.editHtml = window.document.querySelector("#login") as HTMLInputElement;

			const nextButton = this.rulleContainer.getChildByName("NextButton");
			const playButton = this.loginContainer.getChildByName("PlayButton");

			const menButton = this.loginContainer.getChildByName("MenButton") as PIXI.Sprite;
			const womenButton = this.loginContainer.getChildByName("WomenButton") as PIXI.Sprite;
			const menText = this.loginContainer.getChildByName("TextMen") as TiledOG.TiledContainer;
			const womenText = this.loginContainer.getChildByName("TextWomen") as TiledOG.TiledContainer;

			const deselectedTint = 0x5b61b3;
			const textTint = 0xffdc78;
			const defTextTint = menText.text ? menText.text.tint : 0xffffff;

			const selectMen = () => {
				this.app.userConfig.gender = UserGender.BOY;

				menButton.tint = 0xffffff;
				if (menText.text) menText.text.tint = textTint;

				womenButton.tint = deselectedTint;
				if (womenText.text) womenText.text.tint = defTextTint;
			};

			const selectGirl = () => {
				this.app.userConfig.gender = UserGender.GIRL;

				womenButton.tint = 0xffffff;
				if (womenText.text) womenText.text.tint = textTint;

				menButton.tint = deselectedTint;
				if (menText.text) menText.text.tint = defTextTint;
			}

			menButton.on("pointerdown", selectMen);
			womenButton.on("pointerdown", selectGirl);

			if(this.app.userConfig.gender != UserGender.UFO) {
				if(this.app.userConfig.gender == UserGender.BOY) {
					selectMen();
				}else {
					selectGirl();
				}
				this.editHtml.value = this.app.userConfig.name;
			}

			const preset = {
				hover: 0xc2adc6,
				disabled: 0x5b61b3,
				click: 0x865e84
			};

			Utils.registerAsButton(nextButton, preset, () => this.toPersSelector());
			Utils.registerAsButton(playButton, preset, () => this.tryStartGame());
			
			window.addEventListener("resize", () => this.resize());
			
			this.editHtml.addEventListener("input", (e:Event) =>{
				if(this.editHtml)
					this.app.userConfig.name = this.editHtml.value || "";
			})

			const reg = new RegExp(this.editHtml!.pattern);
			this.editHtml!.onkeydown =  (ev:KeyboardEvent) => {
				if(ev.keyCode == 13) {
					this.tryStartGame();
					return false;
				}				
				if(!ev.key) return false;
				return reg.test(ev.key);
			};


			this.resize();
			
		}

		showed(): void {}

		hidden(): void {
			if (this.editHtml) {
				this.editHtml.style.display = "none";
			}
		}
		fadeProgress(p: number, showing: boolean): void {}
        

		update(delta: number): void {}

		private toPersSelector(): void {
			if (this.editHtml) {
				this.editHtml.style.display = "block";
			}

			if (this.loginContainer) this.loginContainer.visible = true;

			if (this.rulleContainer) this.rulleContainer.visible = false;
		}

		private tryStartGame() {
			if (this.app.userConfig.gender == UserGender.UFO && this.selectTitle) {
				Utils.shakingElement(this.selectTitle, .5);
				return;
			}

			if(this.app.userConfig.name.length < 3 && this.editContainer) {
				Utils.shakingElement(this.editContainer,.5, ()=>{
					this.resize();
				});
				return;
			}

			this.app.userConfig.save();
			this.app.layers["game"].init();
			this.app.fadeLayers(this.app.layers["game"], 0.3);

		}

		resize() {
			if (!this.editContainer || !this.editHtml) return;

			Utils.mapTransformToHTML(this.editContainer, this.editHtml, this.app.renderer, {
				preferTextSize: true,
				targetTextSize: 24
			});
		}
	}
}
