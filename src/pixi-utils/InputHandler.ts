namespace InputHandler {
	export let IsKeyDown: Array<boolean> = [];

	let latestDom: HTMLElement | Window | undefined;

	function upHandler(e: any) {
		e.preventDefault();
		IsKeyDown[e.keyCode] = false;
	}

	function downHandler(e: any) {
		e.preventDefault();
		IsKeyDown[e.keyCode] = true;
	}
	
	function focusOut(e: any) {
		IsKeyDown = [];
	}
    
	export function BindKeyHandler(dom: HTMLElement | Window) {
		latestDom = dom;

		dom.addEventListener("keydown", downHandler);
		dom.addEventListener("keyup", upHandler);
		dom.addEventListener("blur", focusOut);
	}

	export function ReleaseKeyHandler() {
		if (latestDom) {
			latestDom.removeEventListener("keydown", downHandler);
			latestDom.removeEventListener("keyup", upHandler);
			latestDom.removeEventListener("blur", focusOut);
			latestDom = undefined;
		}

		IsKeyDown = [];
	}
}
