
namespace PigPoorGame.Objects {
	export class ParticleBuilder {
		layer: GameLayer;
		shitPool: ObjectPool<ShitObject>;
		coinPool: ObjectPool<CoinsObject>;

		pig: BigPig;
		roadLayer: TiledOG.TiledContainer;
		coinLayer: TiledOG.TiledContainer;
		road: TiledOG.TiledContainer;
		player: Player;
		shitProbability: number = 0.3;
		duration: number = 0;

		private _poolGenTimer: number = -1;
		constructor(layer: GameLayer) {
			this.layer = layer;
			this.player = layer.player!;

			this.shitProbability = layer.app.gameConfig.shitProbability || 0.3;
			this.pig = layer.bigPig!;
			this.roadLayer = layer!.stage!.getChildByPath<TiledOG.TiledContainer>("RoadLayer")!;
			this.coinLayer = layer!.stage!.getChildByPath<TiledOG.TiledContainer>("Coins")!;

			this.road = this.roadLayer!.getChildByPath<TiledOG.TiledContainer>("Road")!;

			this.shitPool = new ObjectPool<ShitObject>(
				this.buildShit.bind(this),
				this.preGet.bind(this),
				this.posSet.bind(this)
			);

			const count = layer.app.gameConfig.shitsPerSceen || 7;
			this.shitPool.populate(count, true);

			this.coinPool = new ObjectPool<CoinsObject>(
				this.buildCoins.bind(this),
				this.preGet.bind(this),
				this.posSet.bind(this)
			);
		}

		buildShit(): ShitObject {
			const shit = new ShitObject(this.layer, this.layer.app.gameConfig.shitLabels);
			shit.visible = false;
			this.roadLayer.addChild(shit);
			return shit;
		}

		buildCoins(): CoinsObject {
			const coin = new CoinsObject(this.layer);
			coin.visible = false;
			this.coinLayer.addChild(coin);
			return coin;
		}

		preGet(v: ShitObject | CoinsObject) {
			v.position.copy(this.pig.position);
			v.position.y -= this.pig.height * 0.5;
			v.visible = true;
			v.alpha = 1;

			if (v instanceof CoinsObject) {
				const pos = (v.anim.totalFrames * Math.random()) >> 0;
				v.anim.gotoAndPlay(pos);
			}
		}

		posSet(v: ShitObject | CoinsObject) {
			v.visible = false;
			v.speeds.set(0, 0);

			if (v instanceof ShitObject) {
				v.hideText();
				clearTimeout(v.timeoutHandler);
			}

			if (v instanceof CoinsObject) {
				v.anim.stop();
			}
		}

		start(duration: number = 100): void {
			this.duration = duration;

			this.pause();
			this._poolGenTimer = setInterval(() => {
				this.gen();
			}, duration);
		}

		pause(): void {
			clearInterval(this._poolGenTimer);
		}

		reset(): void {
			this.shitPool.pushAll();
			this.coinPool.pushAll();
		
			this.layer.app.soundManager.Stop("shit_spanv");
			this.layer.app.soundManager.Stop("coin_spanv");
			
			this.pause();
		}

		gen(boost: number = 1) {
			const rnd = Math.random();
			const shitBlinkDuration = 0.5;
			let obj = rnd < this.shitProbability ? this.shitPool.pop() : this.coinPool.pop();

			if(obj instanceof ShitObject) {
			
				this.layer.app.soundManager.Play("shit_spanv");
				const _pool = this.shitPool;
				const liveTime = (this.layer.app.gameConfig.shitsLiveTime | 5)
				var timeout =  liveTime - shitBlinkDuration;
				timeout += (0.5 - Math.random()) * 0.2 * liveTime;
				const shit = obj as ShitObject;
				
				shit.timeoutHandler = setTimeout(function(){
					shit.blink(shitBlinkDuration, ()=>{
						_pool.push(shit);
					});
				}, timeout * 1000);
		
			}else {
			
				this.layer.app.soundManager.Play("coin_spanv");
			
			}


			obj!.speeds.x = -this.pig.direction * (20 + 50 * Math.random()) * boost;
			obj!.speeds.y = -(30 + Math.random() * 100) * boost;
		}

		brust(count: number) {
			while (count > 0) {
				this.gen(2);
				count--;
			}
		}

		update(dt: number) {
			this.updateShits(dt);
			this.updateCoins(dt);
		}

		updateShits(dt: number) {
			if (!this.shitPool.usedArr) return;

			const safeY = this.road.position.y - this.road.height + 10;

			const arr = this.shitPool.usedArr;
			for (let i = arr.length - 1; i >= 0; --i) {
				const obj = arr[i];

				const hit = this.hitTest(obj!.hitObject!, this.player!.hitObject!);
				if(hit) {
					this.shitPool.push(obj);
					this.layer!.shitHitEvent();
					this.layer.app.soundManager.Play("shit_catch");
			
					continue;
				}
				if (obj.position.y < safeY) {
					obj.update(dt);
				} else {
					obj.showText();
				}
			}
		}

		updateCoins(dt: number) {
			if (!this.coinPool.usedArr) return;

			const safeY = this.layer.app.renderer.height + 20;

			const arr = this.coinPool.usedArr;
			for (let i = arr.length - 1; i >= 0; --i) {
				const obj = arr[i];

				const hit = this.hitTest(obj, this.player!.hitObject!);
				
				if(hit) {
					this.layer!.coinHitEvent();
					this.layer.app.soundManager.Play("coin_catch");
				}

				if (obj.position.y < safeY && !hit) {
					obj.update(dt);
				} else {
					this.coinPool.push(obj);
				}
			}
		}

		hitTest(objA: PIXI.DisplayObject, objB: PIXI.DisplayObject): boolean {
			const rectA = objA.getBounds();
			const rectB = objB.getBounds();

			if (rectA.left > rectB.right) return false;
			if (rectA.right < rectB.left) return false;
			if (rectA.bottom < rectB.top) return false;
			if (rectA.top > rectB.bottom) return false;

			return true;
		}
	}
}
