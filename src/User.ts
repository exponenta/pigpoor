
namespace PigPoorGame {

    export enum UserGender {
        UFO, BOY, GIRL 
    }
    
    export interface UserProfile {
		name: string;
		isMen?: boolean;
		gender: UserGender;
		roundDiration?: number;
		score: number;
    }

    export class User implements UserProfile{
        
        private _name: string = "";
        private _gender: UserGender = UserGender.UFO;
    
        roundDiration: number = 0;
        totalCoints: number = 0;
        totalShits: number = 0;
        score: number = 0;

        _uuid: string = "";
        constructor() {
            this._uuid = PIXI.utils.uid().toString(16);
        }

        load() {
            if(Cookies) {
                const conf = Cookies.getJSON("UserConfig") as UserProfile;
                if(conf){
                    this._gender = conf.gender || UserGender.UFO;
                    this._name = conf.name;
                }
            }
        }

        save() {
            if(Cookies) {
				Cookies.set("UserConfig", {
                    gender: this._gender,
                    name: this.name,
                });
			}
        }

        set name(n: string) {
            if(name == n || n == undefined || n.length < 2) return;

            this._name = n;
      
        }
        get name() {
            return this._name;
        }

        set gender(g: UserGender) {
            if(g == this._gender) return;
          
            this._gender = g;
        }

        get gender() {
            return this._gender;
        }

        resetScore(){
            this.score = 0;
            this.totalCoints = 0;
            this.totalShits = 0;
            this.roundDiration = 0;
        }

        formatText(text: string) {
			
			text = text.replace(/\${name}/g, this.name);
			text = text.replace(/\${score}/g, "" + this.score);
			text = text.replace(/\${coints}/g, "" + this.totalCoints);
			text = text.replace(/\${shits}/g, "" + this.totalShits);
			
			var reg = undefined;
			if(this.gender == UserGender.BOY) {
				text = text.replace(/(?:\$if_girl{)(.*?)(?:})/g,"");
				reg = new RegExp(/(?:\$if_boy{)(.*?)(?:})/i);

			}else {
				text = text.replace(/(?:\$if_boy{)(.*?)(?:})/g,"");
				reg = new RegExp(/(?:\$if_girl{)(.*?)(?:})/i);
			}
			var math = undefined;
			do {
				math = reg.exec(text);
				if(math) {
					text = text.replace(math[0], math[1]);
				}
			}while (math)

			return text;
		}
    }
}