declare module PIXI.loaders {
	export interface Loader {
		filter(func: (v: PIXI.loaders.Resource) => boolean): PIXI.loaders.Resource[];
	}
}

PIXI.loaders.Loader.prototype.filter = function(func: (v: PIXI.loaders.Resource) => boolean) {
	if (!func) return [];

	const ress = this.resources;

	let ret: Array<PIXI.loaders.Resource> = [];

	let keys = Object.keys(ress);

	keys.forEach((k: string) => {
		if (func(ress[k])) {
			ret.push(ress[k]);
		}
	});

	return ret;
};
