namespace PigPoorGame.Utils {

	export function registerAsButton(
		button: PIXI.DisplayObject,
		props: { disabled?: number; hover?: number; click?: number },
		click?: () => void
	): void {
		const diffTint = (button as any).tint;
		if (props.hover) {
			button.on("pointerover", () => {
				(button as any).tint = props.hover;
			});

			button.on("pointerout", () => {
				(button as any).tint = diffTint;
			});
		}

		if (props.disabled && !button.interactive) {
			(button as any).tint = props.disabled;
		}

		button.on("pointerdown", () => {
			if (props.click) (button as any).tint = props.click;
		});

		button.on("pointerup", () => {
			if (click) click();
		});
	}

	export function shakingElement(target: PIXI.DisplayObject, duration: number = 1, update?: () => void) {
		const proxy = {
			timer: 0
		};
		const amplitude = 10;
		const loops = 3;
		const start = target.position.x;

		gsap.TweenLite.to(proxy, duration, {
			onUpdate: function() {
				target.position.x = start + Math.sin(Math.PI * 2 * loops * proxy.timer) * amplitude;
				if (update) update();
			},
			timer: 1
		}).play();
	}
	export interface MapOptions {
		preferTextSize: boolean;
		targetTextSize: number;
	}

	export function mapTransformToHTML(
		from: PIXI.DisplayObject,
		to: HTMLElement,
		renderer: PIXI.SystemRenderer,
		options?: MapOptions
	) {
		const props = options || ({} as MapOptions);

		if (!from || !to || !renderer) return;

		var rect = from.getBounds();

		let domRect = renderer.view.getBoundingClientRect();
		let parentDomRect = to.parentElement!.getBoundingClientRect();

		var offsetX = domRect.left - parentDomRect.left;
		var offsetY = domRect.top - parentDomRect.top;

		let scale = renderer.width / domRect.width;

		//console.log(scale);

		rect.x /= renderer.width;
		rect.y /= renderer.height;
		rect.width /= renderer.width;
		rect.height /= renderer.height;

		if (props.preferTextSize) to.style.fontSize = (props.targetTextSize || 24) / scale + "px";

		to.style.position = "absolute";
		to.style.left = rect.x * domRect.width + offsetX + "px";
		to.style.top = rect.y * domRect.height + offsetY + "px";
		to.style.width = rect.width * domRect.width + "px";
		to.style.height = rect.height * domRect.height + "px";
	}

	export function StringPad(num: number, size: number) {
		var s = num + "";
		while (s.length < size) s = "0" + s;
		return s;
	}

	/**
	 * Converts an RGB color value to HSV. Conversion formula
	 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
	 * Assumes r, g, and b are contained in the set [0, 1] and
	 * returns h, s, and v in the set [0, 1].
	 *
	 * @param   Number  r       The red color value
	 * @param   Number  g       The green color value
	 * @param   Number  b       The blue color value
	 * @return  Array           The HSV representation
	 */
	export function rgbToHsv(r: number, g: number, b: number) {
		//r /= 255, g /= 255, b /= 255;

		var max = Math.max(r, g, b),
			min = Math.min(r, g, b);
		var h: number = 0,
			s: number = 0,
			v: number = max;

		var d = max - min;
		s = max == 0 ? 0 : d / max;

		if (max == min) {
			h = 0; // achromatic
		} else {
			switch (max) {
				case r:
					h = (g - b) / d + (g < b ? 6 : 0);
					break;
				case g:
					h = (b - r) / d + 2;
					break;
				case b:
					h = (r - g) / d + 4;
					break;
			}

			h /= 6;
		}

		return [h, s, v];
	}

	/**
	 * Converts an HSV color value to RGB. Conversion formula
	 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
	 * Assumes h, s, and v are contained in the set [0, 1] and
	 * returns r, g, and b in the set [0, 1].
	 *
	 * @param   Number  h       The hue
	 * @param   Number  s       The saturation
	 * @param   Number  v       The value
	 * @return  Array           The RGB representation
	 */
	export function hsvToRgb(h: number, s: number, v: number) {
		var r: number = 0,
			g: number = 0,
			b: number = 0;

		var i = Math.floor(h * 6);
		var f = h * 6 - i;
		var p = v * (1 - s);
		var q = v * (1 - f * s);
		var t = v * (1 - (1 - f) * s);

		switch (i % 6) {
			case 0:
				(r = v), (g = t), (b = p);
				break;
			case 1:
				(r = q), (g = v), (b = p);
				break;
			case 2:
				(r = p), (g = v), (b = t);
				break;
			case 3:
				(r = p), (g = q), (b = v);
				break;
			case 4:
				(r = t), (g = p), (b = v);
				break;
			case 5:
				(r = v), (g = p), (b = q);
				break;
		}

		return [r, g, b];
	}
}
