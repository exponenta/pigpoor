namespace PigPoorGame {
	export class BigPig extends PIXI.Container {
		animator: PIXI.extras.AnimatedSprite;
		direction: number = 1;
		speed: number = 80;
		fluctuationHeight: number = 30;
		fluctuationPeriod: number = 50;
		startPos: PIXI.Point = new PIXI.Point(0, 0);
		safeArea?: PIXI.Rectangle;
        startScale: PIXI.Point = new PIXI.Point(1, 1);

        paused: boolean = false;

		constructor(anim: PIXI.Texture[]) {
			super();

			this.animator = new PIXI.extras.AnimatedSprite(anim);
			this.animator.loop = false;
			this.animator.animationSpeed = 0.2;
            this.animator.anchor.set(0.5, 1);
            this.animator.onComplete = () =>{
                this.paused = false;
            };

			this.addChild(this.animator);
		}

		init(): void {
			this.startPos = this.position.clone();
			this.startScale = this.scale.clone();
        }
        
        public hitTest(point: PIXI.Point): boolean {
            const local = this.toLocal(point);
            
           // console.log(point, local);
            return this.hitArea.contains(local.x, local.y);

        }

		push(): void {
            this.animator.gotoAndPlay(0);
            this.paused = true;
		}

		update(dt: number) {

            if(this.paused) return;

			this.position.x += (this.direction * this.speed * dt) / 60;
			const y = Math.sin(this.position.x / this.fluctuationPeriod) * this.fluctuationHeight;
			this.position.y = this.startPos.y + y;
			this.scale.x = this.startScale.x * this.direction;

			if (this.safeArea) {
				const _x = this.position.x;
				if (_x > this.safeArea.right || _x < this.safeArea.left)
					this.direction = Math.sign(0.5 * (this.safeArea.left + this.safeArea.right) - _x);
			}
        }
	}
}
