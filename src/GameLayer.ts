namespace PigPoorGame {
	
	const P_PARAMS = {
		boy: {
			speed: 320,
			jumpDuration: 0.6
		} as PlayerParams,
		girl: {
			speed: 280,
			jumpDuration: 0.8
		} as PlayerParams,
	}

	export enum GameState {
		Init,
		Anim,
		Game,
		Ending
	}

	export class GameLayer implements ILayer {
		stage?: TiledOG.TiledContainer | undefined;
		app: App;
		player?: Player;
		bigPig?: BigPig;
		state: GameState = GameState.Init;

		private playerStartPos: PIXI.Point = new PIXI.Point(0, 0);
		private pigStartPoint: PIXI.Point = new PIXI.Point(0, 0);
		private gameTimer: number = -1;

		private parallax: ParallaxLayer;
		private ui?: TiledOG.TiledContainer;

		private timerText?: PIXI.Text;
		private budgedText?: PIXI.Text;

		private pushCloud?: PIXI.extras.AnimatedSprite;
		private particleBuilder?: Objects.ParticleBuilder;
		private wasInited: boolean = false;
		private updateScopeFunc: { [name: string]: ((x: number) => void) | undefined } = {};

		constructor(app: App) {
			this.app = app;
			this.parallax = app.layers["parallax"] as ParallaxLayer;
		}

		addToLoader(loader: PIXI.loaders.Loader, callback?: (() => void) | undefined): PIXI.loaders.Loader {
			//loader.add("anims", "./maps/animations.json");
			loader.add("shit", "./maps/gfx/shit.png");
			loader.add("game", "./maps/game.json", () => {
				if (callback) callback();
				this.stage = loader.resources["game"].stage;
				//this.init();
			});

			return loader;
		}

		init(): void {
			if (!this.stage || this.wasInited) return;

			this.wasInited = true;

			const front = this.stage!.getChildByPath<TiledOG.TiledContainer>("Front")!;
			const multi: Tiled.MultiSpritesheet = new Tiled.MultiSpritesheet();
			multi.add(this.app.loader.resources["boy_run_animation"].spritesheet);
			multi.add(this.app.loader.resources["anims"].spritesheet);

			this.ui = this.stage.getChildByPath("UI");
			this.timerText = this.stage.getChildByPath<TiledOG.TiledContainer>("UI/TimerText")!.text;
			this.budgedText = this.stage.getChildByPath<TiledOG.TiledContainer>("UI/BudgetText")!.text;

			const cloud = multi.animations["cloud"];
			this.pushCloud = new PIXI.extras.AnimatedSprite(cloud);
			this.pushCloud.animationSpeed = 0.2;
			this.pushCloud.loop = false;
			this.pushCloud.anchor.set(0.5);

			this.pushCloud.visible = false;

			//player
			this.initPlayer(multi);
			this.playerStartPos = this.player!.position.clone();

			//bigPig
			this.initBigPig(multi);
			this.pigStartPoint = this.bigPig!.position.clone();

			//pools
			this.initPools(multi);

			front!.addChild(this.pushCloud);

			//bind mobile input
			this.bindMobileInput();
		}

		initPools(multi: Tiled.MultiSpritesheet): void {

			this.particleBuilder = new Objects.ParticleBuilder(this);
		}

		initPlayer(multi: Tiled.MultiSpritesheet): void {
			const playerPl = this.stage!.getChildByPath<PIXI.Sprite>("Front/Player");

			const isBoy = this.app.userConfig.gender == UserGender.BOY;

			const runSheet = isBoy ? multi.animations["boy_run"] : multi.animations["girl_run"];
			const jumpSheet = isBoy ? multi.animations["boy_jump"] : multi.animations["girl_jump"];

			const prect = playerPl!.getLocalBounds();

			this.player = new Player(runSheet, jumpSheet, isBoy ? P_PARAMS.boy : P_PARAMS.girl);

			this.player.replaceWithTransform(playerPl!);
			this.player.position.x += playerPl!.width >> 1;

			const area = (playerPl! as any)!.primitive as TiledOG.Primitives.TiledRect;
			area.x -= this.player!.width >> 1;
			area.y -= this.player!.height;

			this.player!.hitArea = area;

			if (this.player!.hitArea) {
				const hit = new PIXI.Graphics();
				hit.lineStyle(2, 0xff0000).drawShape(this.player!.hitArea);
				hit.visible = DEBUG;
				this.player!.hitObject = hit;
				this.player!.addChild(hit);
			}

			this.player.safeArea = new PIXI.Rectangle(50, 0, this.app.renderer.width - 100, 0);

			//playerPl!.destroy();
		}

		initBigPig(multi: Tiled.MultiSpritesheet): void {
			const pig = this.stage!.getChildByPath<PIXI.Sprite>("Front/Pig");
			const prect = pig!.getLocalBounds();

			const anim = multi.animations["pig"];
			this.bigPig = new BigPig(anim);

			this.bigPig.replaceWithTransform(pig!);

			this.bigPig.position.x += pig!.width >> 1;

			const area = this.stage!.getChildByPath<TiledOG.TiledContainer>("Front/PigMovableRegion")!
				.primitive as TiledOG.Primitives.TiledRect;
			area.x += prect.width * 0.5;
			area.width -= prect.width;

			const hitarea = (pig as any)!.primitive as TiledOG.Primitives.TiledEllipse;

			hitarea.x -= (this.bigPig!.width / this.bigPig.scale.x) >> 1;
			hitarea.y -= this.bigPig!.height / this.bigPig.scale.y;

			this.bigPig!.hitArea = hitarea;

			if (DEBUG && this.player!.hitArea) {
				const hit = new PIXI.Graphics();
				hit.lineStyle(2, 0xff0000).drawShape(this.bigPig!.hitArea);
				this.bigPig!.addChild(hit);
			}

			this.bigPig.safeArea = area;

			this.bigPig.init();
			pig!.destroy();
		}

		showed(): void {

			this.app.soundManager.Stop("intro", true);

			const main = this.app.soundManager.sounds["main"];
			if(main.playing()){
				main.fade(.1, .5,2);
			}else {
				this.app.soundManager.Play("main", true,0.5, true);
			}

			this.restart();

			const target = this.stage!.getChildByPath<TiledOG.TiledContainer>("Front/PlayerStartPos")!;
			const xPos = target.x;

			const startDist = Math.abs(xPos - this.player!.x);
			this.player!.direction = Math.sign(xPos - this.player!.x);

			this.state = GameState.Anim;

			this.updateScopeFunc["StartAnimPlayer"] = (dt: number) => {
				const dist = Math.abs(xPos - this.player!.x);
				this.parallax.fader = 1 - dist / startDist;
				this.stage!.y = this.parallax.stage!.y;

				if (dist < 10) {
					this.startGame();
				}
			};
		}

		hidden(): void {
			
			this.stage!.getChildByPath<PIXI.Sprite>("RoadLayer/Road")!.visible = false;
		
		}

		restart() {

			this.particleBuilder!.reset();
			this.app.userConfig.score = 0;
			this.player!.position.copy(this.playerStartPos);
			this.bigPig!.position.copy(this.pigStartPoint);

			this.stage!.getChildByPath<PIXI.Sprite>("RoadLayer/Road")!.visible = true;
			this.stage!.y = this.parallax.stage!.y;
		}

		startGame(): void {
			const duration = this.app.gameConfig.spavnDuration;
			
			if(duration)
				this.particleBuilder!.start(duration * 1000);
	
			this.animateText(0,0);
			this.parallax.fader = 1;
			this.stage!.y = 0;
			this.player!.direction = 0;
			this.updateScopeFunc["StartAnimPlayer"] = undefined;
			this.state = GameState.Game;

			this.ui!.alpha = 0;
			this.ui!.visible = true;
			gsap.TweenLite.to(this.ui!, 0.3, {
				pixi: {
					alpha: 1
				}
			}).play();

			InputHandler.BindKeyHandler(window);

			let timeLeft = this.app.gameConfig.time;
			this.timeTickerEvent(timeLeft);
			this.gameTimer = setInterval(() => {
				timeLeft--;
				if (timeLeft <= 0) {
					this.endGame();
				}
				this.timeTickerEvent(timeLeft);
			}, 1000);
		}

		endGame() {

			this.player!.direction = 0;
			//this.app.soundManager.Stop("main", true);
			this.app.soundManager.sounds["main"].fade(0.5,.1,.5);

			this.app.soundManager.Play("time_end");

			clearInterval(this.gameTimer);

			this.particleBuilder!.reset();
			this.state = GameState.Ending;
			this.ui!.alpha = 0;

			const result = this.app.layers["result"];

			result.init();
			this.app.fadeLayers(result, 0.5);
			InputHandler.ReleaseKeyHandler();

		}

		shitHitEvent(): void {
			const last = this.app.userConfig.score;
			this.app.userConfig.score -= this.app.gameConfig.shitCost;
			if (this.app.userConfig.score < 0) this.app.userConfig.score = 0;
			this.app.userConfig.totalShits ++;

			this.animateText(last, this.app.userConfig.score);
		}

		coinHitEvent(): void {
			const last = this.app.userConfig.score;
			this.app.userConfig.score += this.app.gameConfig.coinCost;
			this.app.userConfig.totalCoints ++;

			this.animateText(last, this.app.userConfig.score);
		}

		animateText(from: number, to: number) {

			let proxy = {
				val:from
			};

			if(to < from) {
				 this.budgedText!.scale.set(1.2);
				 this.budgedText!.tint = 0xff0000;
			}
			gsap.TweenLite.to(proxy, .5, {
				val: to,
				onUpdate: () =>{
					this.budgedText!.text = Utils.StringPad( (proxy.val >> 0), 4);
				}
			}).eventCallback("onComplete", ()=>{
				this.budgedText!.scale.set(1);
				this.budgedText!.tint = 0xFFFFFF;
			}).play();
		}

		pigPushEvent() {
			this.pushCloud!.position = this.player!.headPoint;
			this.pushCloud!.visible = true;
			this.pushCloud!.gotoAndPlay(0);

			this.bigPig!.push();

			this.particleBuilder!.brust(this.app.gameConfig.hitBrust || 5);
			this.particleBuilder!.pause();
			
			this.pushCloud!.onComplete = () => {
				this.pushCloud!.visible = false;
				
				if(this.particleBuilder!.duration){
					this.particleBuilder!.start(this.particleBuilder!.duration);
				}
			};

			this.app.soundManager.Play("pig_punch");
		}

		// ----
		timeTickerEvent(expired: number) {
			if (this.timerText) {
				this.timerText.text = this.formatTime(expired);

				if (expired < 10) {
					gsap.TweenLite.to(this.timerText, 0.5, {
						pixi: {
							tint: 0xff0000,
							scale: 1.2
						}
					})
						.play()
						.eventCallback("onComplete", () => {
							this.timerText!.tint = 0xffffff;
							this.timerText!.scale.set(1);
						});
				}
			}
		}

		formatTime(seconds: number): string {
			const secs = seconds % 60;
			const mins = (seconds / 60) >> 0;
			return `${Utils.StringPad(mins, 2)}:${Utils.StringPad(secs, 2)}`;
		}

		jumpIsRelease: boolean = true;
		update(delta: number): void {
			if (!this.wasInited) return;

			this.particleBuilder!.update(delta);
			this.bigPig!.update(delta);

			this.updatePlayer(delta);
			this.updateInteraction(delta);
			this.updateFuncs(delta);
		}

		updateFuncs(delta: number): void {
			for (const key in this.updateScopeFunc) {
				if (this.updateScopeFunc.hasOwnProperty(key)) {
					const element = this.updateScopeFunc[key];
					if (element) element(delta);
				}
			}
		}

		updatePlayer(delta: number) {
			if (this.state == GameState.Game) {
				this.keyboardInput();
			}

			//update Player
			this.player!.update(delta);

			//updateParallax
			this.parallax!.updateParallax(
				this.player!.x - this.playerStartPos.x,
				this.player!.y - this.playerStartPos.y
			);
		}

		private hitReleased: boolean = true;

		updateInteraction(delta: number): void {
			let top = this.player!.headPoint;
			top = this.stage!.toGlobal(top);

			if (this.bigPig!.hitTest(top) && this.player!.jumping) {
				if (this.hitReleased) {
					this.pigPushEvent();
				}
				this.hitReleased = false;
			} else {
				this.hitReleased = true;
			}
		}

		bindMobileInput(): void {

			const mobile = this.stage!.getChildByPath<TiledOG.TiledContainer>("Mobile");
			mobile!.visible = PIXI.utils.isMobile.any || DEBUG;

			const left = mobile!.getChildByPath<PIXI.Sprite>("Left");

			//emulate left
			left!.on("pointerdown",() => {
				InputHandler.IsKeyDown[37] = true;
			});
			left!.on("pointerup",() => {
				InputHandler.IsKeyDown[37] = false;
			});
			left!.on("pointerupoutside",() => {
				InputHandler.IsKeyDown[37] = false;
			});
			

			const right = mobile!.getChildByPath<PIXI.Sprite>("Right");
		
			//emulate right			
			right!.on("pointerdown",() => {
				InputHandler.IsKeyDown[39] = true;
			});
			right!.on("pointerup",() => {
				InputHandler.IsKeyDown[39] = false;
			});
			right!.on("pointerupoutside",() => {
				InputHandler.IsKeyDown[39] = false;
			});
			

			const jump = mobile!.getChildByPath<PIXI.Sprite>("Jump");
			
			//emulate jump
			jump!.on("pointerdown",() => {
				InputHandler.IsKeyDown[32] = true;
			});
			jump!.on("pointerup",() => {
				InputHandler.IsKeyDown[32] = false;
			});
			jump!.on("pointerupoutside",() => {
				InputHandler.IsKeyDown[32] = false;
			});
			
	
		}

		keyboardInput(): void {
			const leftPressed = ~~InputHandler.IsKeyDown[37];
			const rightPressed = ~~InputHandler.IsKeyDown[39];

			this.player!.direction = rightPressed - leftPressed;

			//jump
			if (InputHandler.IsKeyDown[38] || InputHandler.IsKeyDown[32]) {
				if (this.jumpIsRelease) {
					this.player!.jump();
					this.app.soundManager.Play("jump");
				}
				this.jumpIsRelease = false;
			} else {
				this.jumpIsRelease = true;
			}
		}
	}
}
