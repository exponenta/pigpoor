namespace PigPoorGame {
	export interface PlayerParams {
		speed: number;
		jumpDuration: number;
	}

	export class Player extends PIXI.Container {
		private playerRunAnimator: PIXI.extras.AnimatedSprite;
		private playerJumpAnimator: PIXI.extras.AnimatedSprite;
		private _isJump: boolean = false;
		private _dir: number = 0;
        private _speed: number = 0;
        
		jumpHeight: number = 200;
		jumpDuration: number = 0.6;
		hitObject?: PIXI.DisplayObject;

		safeArea?: PIXI.Rectangle = undefined;
		
		constructor(run: PIXI.Texture[], jump: PIXI.Texture[], params?: PlayerParams ) {
			super();

			this.playerRunAnimator = new PIXI.extras.AnimatedSprite(run, false);
			this.playerJumpAnimator = new PIXI.extras.AnimatedSprite(jump, false);

			this.addChild(this.playerRunAnimator, this.playerJumpAnimator);
            
            this.playerRunAnimator.anchor.x = this.playerJumpAnimator.anchor.x = 0.5;
            this.playerRunAnimator.anchor.y = this.playerJumpAnimator.anchor.y = 1;
            
		    //this.playerJumpAnimator.x = this.playerRunAnimator.x = this.playerRunAnimator.width * 0.5;
            
            this.playerJumpAnimator.play();
            this.playerRunAnimator.play();
			
			this.speed = 300;
			
			this.jumping = false;
			this.direction = 0;

			if(params) {
				this.speed = params.speed;
				this.jumpDuration = params.jumpDuration;
			}
		}

		set speed(s: number) {
			const h = (this.playerRunAnimator.textures[0] as PIXI.Texture).frame.width;
			const rate = s / (h * this.playerRunAnimator.totalFrames);
			this.playerRunAnimator.animationSpeed = rate;
			this._speed = s;
		}
		get speed(): number {
			return this._speed;
        }
        
        get headPoint(): PIXI.Point {
        
            const point = this.position.clone();
            point.y -= (192 - 23);
            return point; 
        }

		private startY: number = 0;

		set jumping(j: boolean) {
			this.playerRunAnimator.visible = !j;
			this.playerJumpAnimator.visible = j;

			if (j && !this._isJump) {
				this.startY = this.position.y;
				this.jumpTimer = 0; //up
			}

			this._isJump = j;
		}

		get jumping(): boolean {
			return this._isJump;
		}

		set direction(direction: number) {
			//if( this._isJump) return;

			if (direction > 0) {
				this.playerJumpAnimator.scale.x = this.playerRunAnimator.scale.x = -1;
			} else if (direction < 0) {
				this.playerJumpAnimator.scale.x = this.playerRunAnimator.scale.x = 1;
			}

			if (!this._isJump) {
				if (direction != this._dir) {
					//if(direction != 0)
					// this.playerRun.play();
					//else
					//    this.playerRun.gotoAndStop(1);
				}
			}

			this._dir = direction;
		}

		get direction() {
			return this._dir;
		}

		jump(): void {
			if (this._isJump) return;
			this.jumping = true;
		}

		update(dt: number) {
			this.position.x += (this.direction * dt * this.speed) / 60;

			if (this.safeArea) {
                const _x = this.x;
                if(_x > this.safeArea.right && this.direction > 0 ||
                    _x < this.safeArea.left && this.direction  < 0) {
                        this.x = Math.max(this.safeArea.left, Math.min(this.x, this.safeArea.right) );
                    }
			}

			if (this.direction != 0) {
				(this.playerRunAnimator as any).update(dt);
			}

			if (this.jumping) {
				this.evaluateJumping(dt);
			}
		}

		private jumpTimer: number = 0;
		evaluateJumping(dt: number): void {
			const tick = dt / 60;
			const delta = tick / this.jumpDuration;

			this.jumpTimer += delta;

			const normalT = 2 * (0.5 - this.jumpTimer);
			const parabl = (1 - normalT * normalT) * this.jumpHeight;
			this.position.y = this.startY - parabl;
			//this.position.y = this.startY - Math.sin(this.jumpTimer * Math.PI) * this.jumpHeight;

			this.playerJumpAnimator.gotoAndStop(Math.round(this.jumpTimer * this.playerJumpAnimator.totalFrames));

			if (this.jumpTimer > 1) {
				this.position.y = this.startY;
				this.jumping = false;
			}
		}
	}
}
