namespace PigPoorGame {
	const zLevelColors: number[] = [0xbaaad2, 0x8169a7, 0x522e8b, 0x551cae];

	export class ParallaxLayer implements ILayer {
		stage?: TiledOG.TiledContainer | undefined;
		app: App;

		pigs: PigPoolElement[] = [];

		overlay: PIXI.Sprite;
		parallaxLayers: TiledOG.TiledContainer[] = [];

		pigRefHeight = 120;
		spavnDuration: number = 3000;
		private _fader: number = 0;

		constructor(app: App) {
			this.app = app;

			this.overlay = new PIXI.Sprite(PIXI.Texture.WHITE);
			this.overlay.tint = 0x0;
			this.overlay.alpha = 0.7;
		}

		addToLoader(loader: PIXI.loaders.Loader, callback?: (() => void) | undefined): PIXI.loaders.Loader {
			loader.add("parallax", "./maps/parallax.json", () => {
				this.stage = loader.resources["parallax"].stage;
			});
			return loader;
		}

		init(): void {
			const prls = ["Front", "City_front", "City_back", "Clouds"];
			this.parallaxLayers = prls.map(name => {
				return this.stage!.getChildByPath<TiledOG.TiledContainer>(name)!;
			});

			const rect = this.stage!.getBounds();

			this.overlay.x = rect.x;
			this.overlay.y = rect.y;
			this.overlay.width = rect.width;
			this.overlay.height = rect.height;

			this.stage!.addChild(this.overlay);

			this.stage!.y = 50;

			// pigs of war
			let pigssheet = this.app.loader.resources["anims"]!.spritesheet!.animations;

			const pigs_anims = ["war_pig_1", "war_pig_2", "war_pig_3", "war_pig_4"];
			const pigs = pigs_anims.map(v => new PIXI.extras.AnimatedSprite(pigssheet[v]));

			pigs.forEach(v => {
				const fh = (v.textures as PIXI.Texture[])[0].frame.height;
				v.scale.set(0.8);
				v.anchor.set(0.5);
				v.animationSpeed = 0.2;
				v.play();
				this.pigs.push(new PigPoolElement(v));
			});

			this.app.stage.addChild(this.stage!);

			this.spavn();
			setInterval(() => {
				this.spavn();
			}, this.spavnDuration);

			this.app.ticker.add((dt: number) => {
				this.update(dt);
			});

            this.updateParallax(0, 0);
		}

		set fader(f: number) {
			f = Math.min(1, Math.max(f, 0));
			this.stage!.y = (1 - f) * 50;
			this.overlay.alpha = (1 - f) * 0.7;
			this._fader = f;
		}

		get fader(): number {
			return this._fader;
		}

		spavn() {
			const rect = this.stage!.getBounds();

			const x = Math.random() > 0.5 ? rect.left : rect.right;
			const ZLevel = (Math.random() * 3) >> 0;
			const y = 100 + (150 + ZLevel * 80) * Math.random();

			const freePigs = this.pigs.filter(p => !p.isUsed);

			if (freePigs.length == 0) return;

			const index = (Math.random() * freePigs.length) >> 0;
			const pig = freePigs[index];

			this.parallaxLayers[ZLevel].addChild(pig);

			pig.position.set(x, y);
			pig.direction = x < 0 ? 1 : -1;
			pig.zScale = 0.3 + (1 - ZLevel / 3.0) * 0.7;
			pig.isUsed = true;

			pig.tint = zLevelColors[ZLevel];

		}

		release(pig: PigPoolElement) {

			if (!pig.isUsed) return;
			pig.parent.removeChild(pig);
            pig.isUsed = false;
            
		}

		showed(): void {}
		hidden(): void {}
		fadeProgress(p: number, showing: boolean): void {}

		update(delta: number): void {
			for (const p of this.pigs) {
				if (p.isUsed) {
					p.update(delta);

					if (
						(p.direction > 0 && p.position.x > this.app.renderer.width + 100) ||
						(p.direction < 0 && p.position.x < -100)
					) {
						this.release(p);
					}
				}
			}
		}

		updateParallax(x: number, y: number) {
			if (!this.stage) return;
			const npos = new PIXI.Point(x, y);

			npos.x /= this.app.renderer.width;
			npos.y /= this.app.renderer.height;

			for (const layer of this.parallaxLayers) {
				let factorX = (layer as any).parallaxFactor;
				if (factorX == undefined) factorX = 100;

				let factorY = (layer as any).parallaxFactorY;
				if (factorY === undefined) factorY = 100;

				layer.position.x = (0.5 - npos.x) * factorX;
				layer.position.y = (0.5 - npos.y) * factorY;
			}
		}
	}
}
