
namespace PigPoorGame.Objects {
	export const SHIT_TEXT_OFFSET = 15;

	export class ShitObject extends PIXI.Container {

		hitObject: PIXI.Graphics;
		sprite?: PIXI.Sprite;
		text?: PIXI.Text;
		textBg?: PIXI.Sprite;
		layer: ILayer;
        shitTextPool: Array<string> = [];
        speeds: PIXI.Point = new PIXI.Point(0,0);
		isShowed: boolean  = false;
		timeoutHandler: number = -1;
		
		constructor(layer: ILayer, text:string[] | undefined) {
			super();

			this.shitTextPool = text || ["NALOG"];

			const loader = layer.app.loader;
			this.layer = layer;

			const shit = loader.filter(v => {
				return v.name.indexOf("shit") > -1;
			})[0].texture;

			const enter_name = loader.filter(v => {
				return v.name.indexOf("enter_name") > -1;
			})[0].texture;

			this.sprite = new PIXI.Sprite(shit!);
			this.sprite.anchor.set(0.5, 1);
			this.textBg = new PIXI.Sprite(enter_name);
			this.textBg.anchor.set(0.5, 0.5);

			this.text = new PIXI.Text("NALOG", {
				fontFamily: "Pixel Cyr, Arial",
				fontSize: 14
			});
			this.text!.anchor.set(0.5, 0.5);

			this.addChild(this.textBg);
			this.addChild(this.text);
			this.addChild(this.sprite);

			const hitRect = this.sprite!.getLocalBounds();
			hitRect.height -= 15;
			hitRect.y +=15;

			this.hitObject = new PIXI.Graphics();
			this.hitObject.lineStyle(2, 0x0000ff);
			this.hitObject.drawShape(hitRect);
			this.hitObject.visible = DEBUG;
			this.addChild(this.hitObject);
		
			this.text.visible = false;
            this.textBg.visible = false;
        
			//this.setText("NDFL 20");
		}

		setText(text: string) {
			this.text!.text = text;

			this.textBg!.width = Math.max(this.text!.width + 16, this.sprite!.width);
			this.textBg!.height = this.text!.height + 8;

			this.textBg!.x = this.text!.x = 0;
			this.textBg!.y = this.text!.y = -(this.sprite!.height + Objects.SHIT_TEXT_OFFSET);
		}

		showText() {
            if(this.isShowed) return;

            this.isShowed = true;

            const id = (this.shitTextPool.length * Math.random()) >> 0;

			this.setText( this.layer.app.userConfig.formatText(this.shitTextPool[id]));

			this.text!.visible = this.textBg!.visible = true;
		}

		hideText() {
            this.isShowed = false;
			this.text!.visible = this.textBg!.visible = false;
		}
		
		blink(duration: number = 3, endCallback: () => (void) | undefined) {
			const blink_times = 5;
			const proxy = {
				val: this.alpha
			};
			gsap.TweenLite.to(proxy, duration, {
				val: 0,
				onUpdate: () => {
					this.alpha = 0.5 + 0.5 * Math.sin(Math.PI * proxy.val * blink_times);
				},
				onComplete: () => {
					if(endCallback)
						endCallback();
				}
			}).play();
		}
        update(dt: number) {

            this.speeds.y += dt * 100 / 60;

            this.position.x += dt * this.speeds.x  / 60;
            this.position.y += dt * this.speeds.y  / 60;
            
        }
	}
}
