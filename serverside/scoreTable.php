<?php
    header("Access-Control-Allow-Origin: ${_SERVER['HTTP_HOST']}");
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

    header('Content-Type: application/json');

    $QUERY_COUNT = 10;

    function getData($conn, $count) {

        $sql = "SELECT * FROM Scores ORDER BY Score DESC LIMIT $count";
        $result = $conn->query($sql);
        $arr = array();

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $arr[] = $row;
            }

            return json_encode($arr);
        } else {
            return json_encode([]);
        }
    }

    function get_ip()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    

    $servername = "sql100.epizy.com";
    $username = "epiz_22882625";
    $password = "A6TA7329klLcwJJ";
    
    $dbname = "epiz_22882625_puffyBudget";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    
    if($_SERVER['REQUEST_METHOD'] === 'GET') {
            
        echo getData($conn);
    }

    if($_SERVER['REQUEST_METHOD'] === 'POST'){

        $name = $_POST["name"];
        $score = $_POST["score"];
        $gender = $_POST["gender"];
        $playingDuration = -1;
        
        if(isset($_POST["playingTime"]))
           $playingDuration =  $_POST["playingTime"];
        
        $ip = get_ip(); 

        if(empty($score)  || empty($name))
        { 
            http_response_code(400);
            echo "{}";
        
        } else {

            $sql = "INSERT INTO Scores (name, score, gender, duration, ip) VALUES ('$name', '$score', '$gender', '$playingDuration', '$ip')";
            
            if ($conn->query($sql) === TRUE) {
                echo getData($conn);
            } else {

                http_response_code(500);
                echo  json_encode($conn->error);
            }
        }
    }

    $conn->close();
