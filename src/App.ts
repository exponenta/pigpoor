/// <reference path="../node_modules/firebase/index.d.ts" />

//lol
(window as any).gsap = {
	TweenLite: (window as any).TweenLite
};


namespace PigPoorGame {
	export const Sounds: ISound[] = [
		{ name: "intro", src: "./sounds/intro.mp3" },
		{ name: "main", src: "./sounds/main.mp3" },

		{ name: "coin_spanv", src: "./sounds/coin2.mp3", preload: true },
		{ name: "coin_catch", src: "./sounds/catch_coin.mp3", preload: true },
		{ name: "shit_spanv", src: "./sounds/shit.mp3", preload: true },
		{ name: "shit_catch", src: "./sounds/shit_2.mp3", preload: true },
		{ name: "time_end", src: "./sounds/end_time.mp3", preload: true },
		{ name: "counter", src: "./sounds/counter.mp3", preload: true },
		{ name: "jump", src: "./sounds/jump_1.mp3", preload: true },
		{ name: "pig_main", src: "./sounds/pig_oink_main.mp3", preload: true },
		{ name: "pig_punch", src: "./sounds/punch_pig3.mp3", preload: true }
	];
	export let DEBUG = false;



	export interface GameConfig {
		time: number;
		shitLabels: Array<string>;
		greetings: Array<string>;
		shitsPerSceen: number;
		shitsLiveTime: number;
		shitProbability: number;
		hitBrust: number;
		coinCost: number;
		shitCost: number;
		spavnDuration: number;
		firebaseConfig: firebase.firestore.Settings | undefined;
		requestUrl: string;
	}

	export class App extends PIXI.Application {
		private updateFunc?: (x: number) => void;

		userConfig: User = new User();
		gameConfig: GameConfig;
		currentLayer?: ILayer;
		loadingLayer: LoadingLayer;
		layers: { [name: string]: ILayer } = {};
		soundManager: SoundManager;
		netManager: NetManager;

		constructor(dom: HTMLCanvasElement, config: GameConfig) {
			super({
				autoStart: false,
				view: dom,
				width: 1000,
				height: 650,
				roundPixels: true
			});

			this.gameConfig = config;
			window.addEventListener("resize", () => this.resize());
			this.resize();
			//PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

			TiledOG.InjectToPixi({
				usePixiDisplay: false,
				roundFontAlpha: false
			});

			this.userConfig.load();
			this.loadingLayer = new LoadingLayer(this);

			//this.layers["loading"] = this.loadingLayer;

			this.layers["parallax"] = new ParallaxLayer(this);
			this.layers["login"] = new LoginLayer(this);
			this.layers["game"] = new GameLayer(this);
			this.layers["result"] = new ResultLayer(this);

			this.soundManager = new SoundManager(Sounds);
			Howler.volume(0.4);

			this.netManager = new NetManager({
				ajaxUrl: config.requestUrl,
				firebase: config.firebaseConfig
			});
			
			// Force fullscreen on mobile
			if (PIXI.utils.isMobile.any || DEBUG) {
				const elem = (this.view.parentElement || this.view) as any;

				const requestFS = () => {
					const enabled = elem.fullscreenEnabled || elem.mozFullscreenEnabled || elem.webkitFullscreenEnabled;

					if (!enabled) {
						if (elem.requestFullscreen) {
							elem.requestFullscreen();
						} else if (elem.mozRequestFullScreen) {
							elem.mozRequestFullScreen();
						} else if (elem.webkitRequestFullscreen) {
							elem.webkitRequestFullscreen();
						}
					}
				};
				document.addEventListener("touchend", requestFS);
			}
			//


		}

		resize() {
			//if (!PIXI.utils.isMobile.any && !DEBUG) {
			//	this.renderer.view.style.width = 1000 / (window.devicePixelRatio || 1) + "px";
			//	this.renderer.view.style.height = 650 / (window.devicePixelRatio || 1) + "px";
			
			//} else {
				this.renderer.view.style.width = window.innerHeight * (1000 / 650) + "px";
				this.renderer.view.style.margin = "0 auto";
			//	}
		}

		start() {
			this.loadingLayer.addToLoader(this.loader).load(() => {
				this.loadingLayer.init();
			});

			this.updateFunc = (dt: number) => {
				this.update(dt);
			};
			this.ticker.add(this.updateFunc);
			super.start();
		}

		stop() {
			if (this.updateFunc) this.ticker.remove(this.updateFunc);

			super.stop();
		}

		update(dt: number) {
			if (this.currentLayer) {
				this.currentLayer.update(dt);
			}
		}

		activateLayer(layer: ILayer) {
			if (this.currentLayer) {
				if (this.currentLayer.hidden) {
					this.currentLayer.hidden();
				}

				if (this.currentLayer.stage) {
					this.stage.removeChild(this.currentLayer.stage);
				}
			}

			this.currentLayer = layer;

			if (this.currentLayer) {
				if (this.currentLayer.stage) {
					this.stage.addChild(this.currentLayer.stage);
				}
				if (this.currentLayer.showed) {
					this.currentLayer.showed();
				}
			}
		}

		fadeLayers(nextLayer: ILayer, duration: number = 1) {
			if (!nextLayer || !nextLayer.stage) return;

			let proxy0 = {
				val: 0
			};

			nextLayer.stage.alpha = 0;
			this.stage.addChild(nextLayer.stage);

			if (nextLayer.beforeShowed) {
				nextLayer.beforeShowed();
			}

			gsap.TweenLite.to(proxy0, duration, {
				val: 1,
				onUpdate: () => {
					nextLayer.stage!.alpha = proxy0.val;
					if (nextLayer.fadeProgress) {
						nextLayer.fadeProgress(proxy0.val, true);
					}
				}
			})
				.play()
				.eventCallback("onComplete", () => {
					this.currentLayer = nextLayer;
					if (this.currentLayer.showed) {
						this.currentLayer.showed();
					}
				});

			//debugger
			const last = this.currentLayer;

			let proxy1 = {
				val: 1
			};

			if (last && last.stage) {
				if (last.hidden) {
					last.hidden();
				}

				const ls = last.stage;
				gsap.TweenLite.to(proxy1, duration, {
					val: 0,

					onUpdate: () => {
						ls.alpha = proxy1.val;
						if (last.fadeProgress) {
							last.fadeProgress(proxy1.val, false);
						}
					}
				})
					.play()
					.eventCallback("onComplete", () => {
						if (ls) {
							this.stage.removeChild(ls);
							ls.alpha = 1;
						}
					});

				this.currentLayer = undefined;
			}
		}

		destroy() {
			super.destroy(true, {
				children: true,
				baseTexture: true,
				texture: true
			});
		}
	}
}
