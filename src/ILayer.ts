
namespace PigPoorGame {
    export interface ILayer {
        stage? : TiledOG.TiledContainer;
        app: PigPoorGame.App;
        addToLoader(loader:PIXI.loaders.Loader, callback? : () => void):PIXI.loaders.Loader
        init():void
        fadeProgress?(p:number, showing: boolean):void
        beforeShowed?():void
        showed?():void
        hidden?():void
        update(delta:number):void;

    }
}