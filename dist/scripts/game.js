"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="../node_modules/firebase/index.d.ts" />
//lol
window.gsap = {
    TweenLite: window.TweenLite
};
var PigPoorGame;
(function (PigPoorGame) {
    PigPoorGame.Sounds = [
        { name: "intro", src: "./sounds/intro.mp3" },
        { name: "main", src: "./sounds/main.mp3" },
        { name: "coin_spanv", src: "./sounds/coin2.mp3", preload: true },
        { name: "coin_catch", src: "./sounds/catch_coin.mp3", preload: true },
        { name: "shit_spanv", src: "./sounds/shit.mp3", preload: true },
        { name: "shit_catch", src: "./sounds/shit_2.mp3", preload: true },
        { name: "time_end", src: "./sounds/end_time.mp3", preload: true },
        { name: "counter", src: "./sounds/counter.mp3", preload: true },
        { name: "jump", src: "./sounds/jump_1.mp3", preload: true },
        { name: "pig_main", src: "./sounds/pig_oink_main.mp3", preload: true },
        { name: "pig_punch", src: "./sounds/punch_pig3.mp3", preload: true }
    ];
    PigPoorGame.DEBUG = false;
    var App = /** @class */ (function (_super) {
        __extends(App, _super);
        function App(dom, config) {
            var _this = _super.call(this, {
                autoStart: false,
                view: dom,
                width: 1000,
                height: 650,
                roundPixels: true
            }) || this;
            _this.userConfig = new PigPoorGame.User();
            _this.layers = {};
            _this.gameConfig = config;
            window.addEventListener("resize", function () { return _this.resize(); });
            _this.resize();
            //PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
            TiledOG.InjectToPixi({
                usePixiDisplay: false,
                roundFontAlpha: false
            });
            _this.userConfig.load();
            _this.loadingLayer = new PigPoorGame.LoadingLayer(_this);
            //this.layers["loading"] = this.loadingLayer;
            _this.layers["parallax"] = new PigPoorGame.ParallaxLayer(_this);
            _this.layers["login"] = new PigPoorGame.LoginLayer(_this);
            _this.layers["game"] = new PigPoorGame.GameLayer(_this);
            _this.layers["result"] = new PigPoorGame.ResultLayer(_this);
            _this.soundManager = new PigPoorGame.SoundManager(PigPoorGame.Sounds);
            Howler.volume(0.4);
            _this.netManager = new PigPoorGame.NetManager({
                ajaxUrl: config.requestUrl,
                firebase: config.firebaseConfig
            });
            // Force fullscreen on mobile
            if (PIXI.utils.isMobile.any || PigPoorGame.DEBUG) {
                var elem_1 = (_this.view.parentElement || _this.view);
                var requestFS = function () {
                    var enabled = elem_1.fullscreenEnabled || elem_1.mozFullscreenEnabled || elem_1.webkitFullscreenEnabled;
                    if (!enabled) {
                        if (elem_1.requestFullscreen) {
                            elem_1.requestFullscreen();
                        }
                        else if (elem_1.mozRequestFullScreen) {
                            elem_1.mozRequestFullScreen();
                        }
                        else if (elem_1.webkitRequestFullscreen) {
                            elem_1.webkitRequestFullscreen();
                        }
                    }
                };
                document.addEventListener("touchend", requestFS);
            }
            return _this;
            //
        }
        App.prototype.resize = function () {
            //if (!PIXI.utils.isMobile.any && !DEBUG) {
            //	this.renderer.view.style.width = 1000 / (window.devicePixelRatio || 1) + "px";
            //	this.renderer.view.style.height = 650 / (window.devicePixelRatio || 1) + "px";
            //} else {
            this.renderer.view.style.width = window.innerHeight * (1000 / 650) + "px";
            this.renderer.view.style.margin = "0 auto";
            //	}
        };
        App.prototype.start = function () {
            var _this = this;
            this.loadingLayer.addToLoader(this.loader).load(function () {
                _this.loadingLayer.init();
            });
            this.updateFunc = function (dt) {
                _this.update(dt);
            };
            this.ticker.add(this.updateFunc);
            _super.prototype.start.call(this);
        };
        App.prototype.stop = function () {
            if (this.updateFunc)
                this.ticker.remove(this.updateFunc);
            _super.prototype.stop.call(this);
        };
        App.prototype.update = function (dt) {
            if (this.currentLayer) {
                this.currentLayer.update(dt);
            }
        };
        App.prototype.activateLayer = function (layer) {
            if (this.currentLayer) {
                if (this.currentLayer.hidden) {
                    this.currentLayer.hidden();
                }
                if (this.currentLayer.stage) {
                    this.stage.removeChild(this.currentLayer.stage);
                }
            }
            this.currentLayer = layer;
            if (this.currentLayer) {
                if (this.currentLayer.stage) {
                    this.stage.addChild(this.currentLayer.stage);
                }
                if (this.currentLayer.showed) {
                    this.currentLayer.showed();
                }
            }
        };
        App.prototype.fadeLayers = function (nextLayer, duration) {
            var _this = this;
            if (duration === void 0) { duration = 1; }
            if (!nextLayer || !nextLayer.stage)
                return;
            var proxy0 = {
                val: 0
            };
            nextLayer.stage.alpha = 0;
            this.stage.addChild(nextLayer.stage);
            if (nextLayer.beforeShowed) {
                nextLayer.beforeShowed();
            }
            gsap.TweenLite.to(proxy0, duration, {
                val: 1,
                onUpdate: function () {
                    nextLayer.stage.alpha = proxy0.val;
                    if (nextLayer.fadeProgress) {
                        nextLayer.fadeProgress(proxy0.val, true);
                    }
                }
            })
                .play()
                .eventCallback("onComplete", function () {
                _this.currentLayer = nextLayer;
                if (_this.currentLayer.showed) {
                    _this.currentLayer.showed();
                }
            });
            //debugger
            var last = this.currentLayer;
            var proxy1 = {
                val: 1
            };
            if (last && last.stage) {
                if (last.hidden) {
                    last.hidden();
                }
                var ls_1 = last.stage;
                gsap.TweenLite.to(proxy1, duration, {
                    val: 0,
                    onUpdate: function () {
                        ls_1.alpha = proxy1.val;
                        if (last.fadeProgress) {
                            last.fadeProgress(proxy1.val, false);
                        }
                    }
                })
                    .play()
                    .eventCallback("onComplete", function () {
                    if (ls_1) {
                        _this.stage.removeChild(ls_1);
                        ls_1.alpha = 1;
                    }
                });
                this.currentLayer = undefined;
            }
        };
        App.prototype.destroy = function () {
            _super.prototype.destroy.call(this, true, {
                children: true,
                baseTexture: true,
                texture: true
            });
        };
        return App;
    }(PIXI.Application));
    PigPoorGame.App = App;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var BigPig = /** @class */ (function (_super) {
        __extends(BigPig, _super);
        function BigPig(anim) {
            var _this = _super.call(this) || this;
            _this.direction = 1;
            _this.speed = 80;
            _this.fluctuationHeight = 30;
            _this.fluctuationPeriod = 50;
            _this.startPos = new PIXI.Point(0, 0);
            _this.startScale = new PIXI.Point(1, 1);
            _this.paused = false;
            _this.animator = new PIXI.extras.AnimatedSprite(anim);
            _this.animator.loop = false;
            _this.animator.animationSpeed = 0.2;
            _this.animator.anchor.set(0.5, 1);
            _this.animator.onComplete = function () {
                _this.paused = false;
            };
            _this.addChild(_this.animator);
            return _this;
        }
        BigPig.prototype.init = function () {
            this.startPos = this.position.clone();
            this.startScale = this.scale.clone();
        };
        BigPig.prototype.hitTest = function (point) {
            var local = this.toLocal(point);
            // console.log(point, local);
            return this.hitArea.contains(local.x, local.y);
        };
        BigPig.prototype.push = function () {
            this.animator.gotoAndPlay(0);
            this.paused = true;
        };
        BigPig.prototype.update = function (dt) {
            if (this.paused)
                return;
            this.position.x += (this.direction * this.speed * dt) / 60;
            var y = Math.sin(this.position.x / this.fluctuationPeriod) * this.fluctuationHeight;
            this.position.y = this.startPos.y + y;
            this.scale.x = this.startScale.x * this.direction;
            if (this.safeArea) {
                var _x = this.position.x;
                if (_x > this.safeArea.right || _x < this.safeArea.left)
                    this.direction = Math.sign(0.5 * (this.safeArea.left + this.safeArea.right) - _x);
            }
        };
        return BigPig;
    }(PIXI.Container));
    PigPoorGame.BigPig = BigPig;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var P_PARAMS = {
        boy: {
            speed: 320,
            jumpDuration: 0.6
        },
        girl: {
            speed: 280,
            jumpDuration: 0.8
        },
    };
    var GameState;
    (function (GameState) {
        GameState[GameState["Init"] = 0] = "Init";
        GameState[GameState["Anim"] = 1] = "Anim";
        GameState[GameState["Game"] = 2] = "Game";
        GameState[GameState["Ending"] = 3] = "Ending";
    })(GameState = PigPoorGame.GameState || (PigPoorGame.GameState = {}));
    var GameLayer = /** @class */ (function () {
        function GameLayer(app) {
            this.state = GameState.Init;
            this.playerStartPos = new PIXI.Point(0, 0);
            this.pigStartPoint = new PIXI.Point(0, 0);
            this.gameTimer = -1;
            this.wasInited = false;
            this.updateScopeFunc = {};
            this.jumpIsRelease = true;
            this.hitReleased = true;
            this.app = app;
            this.parallax = app.layers["parallax"];
        }
        GameLayer.prototype.addToLoader = function (loader, callback) {
            var _this = this;
            //loader.add("anims", "./maps/animations.json");
            loader.add("shit", "./maps/gfx/shit.png");
            loader.add("game", "./maps/game.json", function () {
                if (callback)
                    callback();
                _this.stage = loader.resources["game"].stage;
                //this.init();
            });
            return loader;
        };
        GameLayer.prototype.init = function () {
            if (!this.stage || this.wasInited)
                return;
            this.wasInited = true;
            var front = this.stage.getChildByPath("Front");
            var multi = new Tiled.MultiSpritesheet();
            multi.add(this.app.loader.resources["boy_run_animation"].spritesheet);
            multi.add(this.app.loader.resources["anims"].spritesheet);
            this.ui = this.stage.getChildByPath("UI");
            this.timerText = this.stage.getChildByPath("UI/TimerText").text;
            this.budgedText = this.stage.getChildByPath("UI/BudgetText").text;
            var cloud = multi.animations["cloud"];
            this.pushCloud = new PIXI.extras.AnimatedSprite(cloud);
            this.pushCloud.animationSpeed = 0.2;
            this.pushCloud.loop = false;
            this.pushCloud.anchor.set(0.5);
            this.pushCloud.visible = false;
            //player
            this.initPlayer(multi);
            this.playerStartPos = this.player.position.clone();
            //bigPig
            this.initBigPig(multi);
            this.pigStartPoint = this.bigPig.position.clone();
            //pools
            this.initPools(multi);
            front.addChild(this.pushCloud);
            //bind mobile input
            this.bindMobileInput();
        };
        GameLayer.prototype.initPools = function (multi) {
            this.particleBuilder = new PigPoorGame.Objects.ParticleBuilder(this);
        };
        GameLayer.prototype.initPlayer = function (multi) {
            var playerPl = this.stage.getChildByPath("Front/Player");
            var isBoy = this.app.userConfig.gender == PigPoorGame.UserGender.BOY;
            var runSheet = isBoy ? multi.animations["boy_run"] : multi.animations["girl_run"];
            var jumpSheet = isBoy ? multi.animations["boy_jump"] : multi.animations["girl_jump"];
            var prect = playerPl.getLocalBounds();
            this.player = new PigPoorGame.Player(runSheet, jumpSheet, isBoy ? P_PARAMS.boy : P_PARAMS.girl);
            this.player.replaceWithTransform(playerPl);
            this.player.position.x += playerPl.width >> 1;
            var area = playerPl.primitive;
            area.x -= this.player.width >> 1;
            area.y -= this.player.height;
            this.player.hitArea = area;
            if (this.player.hitArea) {
                var hit = new PIXI.Graphics();
                hit.lineStyle(2, 0xff0000).drawShape(this.player.hitArea);
                hit.visible = PigPoorGame.DEBUG;
                this.player.hitObject = hit;
                this.player.addChild(hit);
            }
            this.player.safeArea = new PIXI.Rectangle(50, 0, this.app.renderer.width - 100, 0);
            //playerPl!.destroy();
        };
        GameLayer.prototype.initBigPig = function (multi) {
            var pig = this.stage.getChildByPath("Front/Pig");
            var prect = pig.getLocalBounds();
            var anim = multi.animations["pig"];
            this.bigPig = new PigPoorGame.BigPig(anim);
            this.bigPig.replaceWithTransform(pig);
            this.bigPig.position.x += pig.width >> 1;
            var area = this.stage.getChildByPath("Front/PigMovableRegion")
                .primitive;
            area.x += prect.width * 0.5;
            area.width -= prect.width;
            var hitarea = pig.primitive;
            hitarea.x -= (this.bigPig.width / this.bigPig.scale.x) >> 1;
            hitarea.y -= this.bigPig.height / this.bigPig.scale.y;
            this.bigPig.hitArea = hitarea;
            if (PigPoorGame.DEBUG && this.player.hitArea) {
                var hit = new PIXI.Graphics();
                hit.lineStyle(2, 0xff0000).drawShape(this.bigPig.hitArea);
                this.bigPig.addChild(hit);
            }
            this.bigPig.safeArea = area;
            this.bigPig.init();
            pig.destroy();
        };
        GameLayer.prototype.showed = function () {
            var _this = this;
            this.app.soundManager.Stop("intro", true);
            var main = this.app.soundManager.sounds["main"];
            if (main.playing()) {
                main.fade(.1, .5, 2);
            }
            else {
                this.app.soundManager.Play("main", true, 0.5, true);
            }
            this.restart();
            var target = this.stage.getChildByPath("Front/PlayerStartPos");
            var xPos = target.x;
            var startDist = Math.abs(xPos - this.player.x);
            this.player.direction = Math.sign(xPos - this.player.x);
            this.state = GameState.Anim;
            this.updateScopeFunc["StartAnimPlayer"] = function (dt) {
                var dist = Math.abs(xPos - _this.player.x);
                _this.parallax.fader = 1 - dist / startDist;
                _this.stage.y = _this.parallax.stage.y;
                if (dist < 10) {
                    _this.startGame();
                }
            };
        };
        GameLayer.prototype.hidden = function () {
            this.stage.getChildByPath("RoadLayer/Road").visible = false;
        };
        GameLayer.prototype.restart = function () {
            this.particleBuilder.reset();
            this.app.userConfig.score = 0;
            this.player.position.copy(this.playerStartPos);
            this.bigPig.position.copy(this.pigStartPoint);
            this.stage.getChildByPath("RoadLayer/Road").visible = true;
            this.stage.y = this.parallax.stage.y;
        };
        GameLayer.prototype.startGame = function () {
            var _this = this;
            var duration = this.app.gameConfig.spavnDuration;
            if (duration)
                this.particleBuilder.start(duration * 1000);
            this.animateText(0, 0);
            this.parallax.fader = 1;
            this.stage.y = 0;
            this.player.direction = 0;
            this.updateScopeFunc["StartAnimPlayer"] = undefined;
            this.state = GameState.Game;
            this.ui.alpha = 0;
            this.ui.visible = true;
            gsap.TweenLite.to(this.ui, 0.3, {
                pixi: {
                    alpha: 1
                }
            }).play();
            InputHandler.BindKeyHandler(window);
            var timeLeft = this.app.gameConfig.time;
            this.timeTickerEvent(timeLeft);
            this.gameTimer = setInterval(function () {
                timeLeft--;
                if (timeLeft <= 0) {
                    _this.endGame();
                }
                _this.timeTickerEvent(timeLeft);
            }, 1000);
        };
        GameLayer.prototype.endGame = function () {
            this.player.direction = 0;
            //this.app.soundManager.Stop("main", true);
            this.app.soundManager.sounds["main"].fade(0.5, .1, .5);
            this.app.soundManager.Play("time_end");
            clearInterval(this.gameTimer);
            this.particleBuilder.reset();
            this.state = GameState.Ending;
            this.ui.alpha = 0;
            var result = this.app.layers["result"];
            result.init();
            this.app.fadeLayers(result, 0.5);
            InputHandler.ReleaseKeyHandler();
        };
        GameLayer.prototype.shitHitEvent = function () {
            var last = this.app.userConfig.score;
            this.app.userConfig.score -= this.app.gameConfig.shitCost;
            if (this.app.userConfig.score < 0)
                this.app.userConfig.score = 0;
            this.app.userConfig.totalShits++;
            this.animateText(last, this.app.userConfig.score);
        };
        GameLayer.prototype.coinHitEvent = function () {
            var last = this.app.userConfig.score;
            this.app.userConfig.score += this.app.gameConfig.coinCost;
            this.app.userConfig.totalCoints++;
            this.animateText(last, this.app.userConfig.score);
        };
        GameLayer.prototype.animateText = function (from, to) {
            var _this = this;
            var proxy = {
                val: from
            };
            if (to < from) {
                this.budgedText.scale.set(1.2);
                this.budgedText.tint = 0xff0000;
            }
            gsap.TweenLite.to(proxy, .5, {
                val: to,
                onUpdate: function () {
                    _this.budgedText.text = PigPoorGame.Utils.StringPad((proxy.val >> 0), 4);
                }
            }).eventCallback("onComplete", function () {
                _this.budgedText.scale.set(1);
                _this.budgedText.tint = 0xFFFFFF;
            }).play();
        };
        GameLayer.prototype.pigPushEvent = function () {
            var _this = this;
            this.pushCloud.position = this.player.headPoint;
            this.pushCloud.visible = true;
            this.pushCloud.gotoAndPlay(0);
            this.bigPig.push();
            this.particleBuilder.brust(this.app.gameConfig.hitBrust || 5);
            this.particleBuilder.pause();
            this.pushCloud.onComplete = function () {
                _this.pushCloud.visible = false;
                if (_this.particleBuilder.duration) {
                    _this.particleBuilder.start(_this.particleBuilder.duration);
                }
            };
            this.app.soundManager.Play("pig_punch");
        };
        // ----
        GameLayer.prototype.timeTickerEvent = function (expired) {
            var _this = this;
            if (this.timerText) {
                this.timerText.text = this.formatTime(expired);
                if (expired < 10) {
                    gsap.TweenLite.to(this.timerText, 0.5, {
                        pixi: {
                            tint: 0xff0000,
                            scale: 1.2
                        }
                    })
                        .play()
                        .eventCallback("onComplete", function () {
                        _this.timerText.tint = 0xffffff;
                        _this.timerText.scale.set(1);
                    });
                }
            }
        };
        GameLayer.prototype.formatTime = function (seconds) {
            var secs = seconds % 60;
            var mins = (seconds / 60) >> 0;
            return PigPoorGame.Utils.StringPad(mins, 2) + ":" + PigPoorGame.Utils.StringPad(secs, 2);
        };
        GameLayer.prototype.update = function (delta) {
            if (!this.wasInited)
                return;
            this.particleBuilder.update(delta);
            this.bigPig.update(delta);
            this.updatePlayer(delta);
            this.updateInteraction(delta);
            this.updateFuncs(delta);
        };
        GameLayer.prototype.updateFuncs = function (delta) {
            for (var key in this.updateScopeFunc) {
                if (this.updateScopeFunc.hasOwnProperty(key)) {
                    var element = this.updateScopeFunc[key];
                    if (element)
                        element(delta);
                }
            }
        };
        GameLayer.prototype.updatePlayer = function (delta) {
            if (this.state == GameState.Game) {
                this.keyboardInput();
            }
            //update Player
            this.player.update(delta);
            //updateParallax
            this.parallax.updateParallax(this.player.x - this.playerStartPos.x, this.player.y - this.playerStartPos.y);
        };
        GameLayer.prototype.updateInteraction = function (delta) {
            var top = this.player.headPoint;
            top = this.stage.toGlobal(top);
            if (this.bigPig.hitTest(top) && this.player.jumping) {
                if (this.hitReleased) {
                    this.pigPushEvent();
                }
                this.hitReleased = false;
            }
            else {
                this.hitReleased = true;
            }
        };
        GameLayer.prototype.bindMobileInput = function () {
            var mobile = this.stage.getChildByPath("Mobile");
            mobile.visible = PIXI.utils.isMobile.any || PigPoorGame.DEBUG;
            var left = mobile.getChildByPath("Left");
            //emulate left
            left.on("pointerdown", function () {
                InputHandler.IsKeyDown[37] = true;
            });
            left.on("pointerup", function () {
                InputHandler.IsKeyDown[37] = false;
            });
            left.on("pointerupoutside", function () {
                InputHandler.IsKeyDown[37] = false;
            });
            var right = mobile.getChildByPath("Right");
            //emulate right			
            right.on("pointerdown", function () {
                InputHandler.IsKeyDown[39] = true;
            });
            right.on("pointerup", function () {
                InputHandler.IsKeyDown[39] = false;
            });
            right.on("pointerupoutside", function () {
                InputHandler.IsKeyDown[39] = false;
            });
            var jump = mobile.getChildByPath("Jump");
            //emulate jump
            jump.on("pointerdown", function () {
                InputHandler.IsKeyDown[32] = true;
            });
            jump.on("pointerup", function () {
                InputHandler.IsKeyDown[32] = false;
            });
            jump.on("pointerupoutside", function () {
                InputHandler.IsKeyDown[32] = false;
            });
        };
        GameLayer.prototype.keyboardInput = function () {
            var leftPressed = ~~InputHandler.IsKeyDown[37];
            var rightPressed = ~~InputHandler.IsKeyDown[39];
            this.player.direction = rightPressed - leftPressed;
            //jump
            if (InputHandler.IsKeyDown[38] || InputHandler.IsKeyDown[32]) {
                if (this.jumpIsRelease) {
                    this.player.jump();
                    this.app.soundManager.Play("jump");
                }
                this.jumpIsRelease = false;
            }
            else {
                this.jumpIsRelease = true;
            }
        };
        return GameLayer;
    }());
    PigPoorGame.GameLayer = GameLayer;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var LoadingLayer = /** @class */ (function () {
        function LoadingLayer(app) {
            this.app = app;
        }
        LoadingLayer.prototype.addToLoader = function (loader, callback) {
            this.loader = loader;
            loader.add("boy_run_animation", "./maps/preload.json");
            loader.add("loading", "./maps/loading_layer.json", function () {
                if (callback)
                    callback();
            });
            return loader;
        };
        LoadingLayer.prototype.init = function () {
            if (!this.loader)
                return;
            this.stage = this.loader.resources["loading"].stage;
            var sprites = this.loader.resources["boy_run_animation"].spritesheet;
            if (sprites && this.stage) {
                var anim = new PIXI.extras.AnimatedSprite(sprites.animations["boy_run"]);
                anim.loop = true;
                anim.animationSpeed = 0.2;
                anim.play();
                var loading_anim = this.stage.getChildByPath("LoadingScreen/Loading_Anim");
                if (loading_anim) {
                    anim.replaceWithTransform(loading_anim);
                    anim.anchor.copy(loading_anim.anchor);
                    loading_anim.destroy();
                }
            }
            this.app.activateLayer(this);
        };
        LoadingLayer.prototype.showed = function () {
            this.loadNext();
        };
        LoadingLayer.prototype.hidden = function () { };
        LoadingLayer.prototype.fadeProgress = function (p, showing) { };
        LoadingLayer.prototype.update = function (delta) {
        };
        LoadingLayer.prototype.loadNext = function () {
            var _this = this;
            if (!this.loader)
                return;
            var intr = this.app.soundManager.Play("intro", true, 0.5);
            var nextLayer = this.app.layers["login"];
            /*
            if (PigPoorGame.DEBUG) {
                this.app.userConfig.isMen = Math.random() > 0.5;
                this.app.userConfig.name = "TEST";
                this.app.userConfig.uuid = PIXI.utils.uid() + "";

                nextLayer = this.app.layers["game"];
            } else {
                nextLayer = this.app.layers["login"];
            }*/
            var parallax = this.app.layers["parallax"];
            parallax.addToLoader(this.loader);
            this.app.layers["game"].addToLoader(this.loader);
            this.app.layers["login"].addToLoader(this.loader);
            this.app.layers["result"].addToLoader(this.loader);
            this.loader.add("anims", "./maps/animations.json");
            this.loader.load(function () {
                parallax.init();
                nextLayer.init();
                _this.app.activateLayer(nextLayer);
            });
        };
        return LoadingLayer;
    }());
    PigPoorGame.LoadingLayer = LoadingLayer;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var LoginLayer = /** @class */ (function () {
        function LoginLayer(app) {
            this.app = app;
        }
        LoginLayer.prototype.addToLoader = function (loader, callback) {
            var _this = this;
            loader.add("login", "./maps/login.json", function () {
                _this.stage = loader.resources["login"].stage;
            });
            return loader;
        };
        LoginLayer.prototype.init = function () {
            var _this = this;
            if (!this.stage)
                return;
            this.loginContainer = this.stage.getChildByName("Login");
            this.rulleContainer = this.stage.getChildByName("Rulles");
            this.selectTitle = this.loginContainer.getChildByName("SelectPers");
            this.editContainer = this.loginContainer.getChildByName("EditEnterName");
            this.editHtml = window.document.querySelector("#login");
            var nextButton = this.rulleContainer.getChildByName("NextButton");
            var playButton = this.loginContainer.getChildByName("PlayButton");
            var menButton = this.loginContainer.getChildByName("MenButton");
            var womenButton = this.loginContainer.getChildByName("WomenButton");
            var menText = this.loginContainer.getChildByName("TextMen");
            var womenText = this.loginContainer.getChildByName("TextWomen");
            var deselectedTint = 0x5b61b3;
            var textTint = 0xffdc78;
            var defTextTint = menText.text ? menText.text.tint : 0xffffff;
            var selectMen = function () {
                _this.app.userConfig.gender = PigPoorGame.UserGender.BOY;
                menButton.tint = 0xffffff;
                if (menText.text)
                    menText.text.tint = textTint;
                womenButton.tint = deselectedTint;
                if (womenText.text)
                    womenText.text.tint = defTextTint;
            };
            var selectGirl = function () {
                _this.app.userConfig.gender = PigPoorGame.UserGender.GIRL;
                womenButton.tint = 0xffffff;
                if (womenText.text)
                    womenText.text.tint = textTint;
                menButton.tint = deselectedTint;
                if (menText.text)
                    menText.text.tint = defTextTint;
            };
            menButton.on("pointerdown", selectMen);
            womenButton.on("pointerdown", selectGirl);
            if (this.app.userConfig.gender != PigPoorGame.UserGender.UFO) {
                if (this.app.userConfig.gender == PigPoorGame.UserGender.BOY) {
                    selectMen();
                }
                else {
                    selectGirl();
                }
                this.editHtml.value = this.app.userConfig.name;
            }
            var preset = {
                hover: 0xc2adc6,
                disabled: 0x5b61b3,
                click: 0x865e84
            };
            PigPoorGame.Utils.registerAsButton(nextButton, preset, function () { return _this.toPersSelector(); });
            PigPoorGame.Utils.registerAsButton(playButton, preset, function () { return _this.tryStartGame(); });
            window.addEventListener("resize", function () { return _this.resize(); });
            this.editHtml.addEventListener("input", function (e) {
                if (_this.editHtml)
                    _this.app.userConfig.name = _this.editHtml.value || "";
            });
            var reg = new RegExp(this.editHtml.pattern);
            this.editHtml.onkeydown = function (ev) {
                if (ev.keyCode == 13) {
                    _this.tryStartGame();
                    return false;
                }
                if (!ev.key)
                    return false;
                return reg.test(ev.key);
            };
            this.resize();
        };
        LoginLayer.prototype.showed = function () { };
        LoginLayer.prototype.hidden = function () {
            if (this.editHtml) {
                this.editHtml.style.display = "none";
            }
        };
        LoginLayer.prototype.fadeProgress = function (p, showing) { };
        LoginLayer.prototype.update = function (delta) { };
        LoginLayer.prototype.toPersSelector = function () {
            if (this.editHtml) {
                this.editHtml.style.display = "block";
            }
            if (this.loginContainer)
                this.loginContainer.visible = true;
            if (this.rulleContainer)
                this.rulleContainer.visible = false;
        };
        LoginLayer.prototype.tryStartGame = function () {
            var _this = this;
            if (this.app.userConfig.gender == PigPoorGame.UserGender.UFO && this.selectTitle) {
                PigPoorGame.Utils.shakingElement(this.selectTitle, .5);
                return;
            }
            if (this.app.userConfig.name.length < 3 && this.editContainer) {
                PigPoorGame.Utils.shakingElement(this.editContainer, .5, function () {
                    _this.resize();
                });
                return;
            }
            this.app.userConfig.save();
            this.app.layers["game"].init();
            this.app.fadeLayers(this.app.layers["game"], 0.3);
        };
        LoginLayer.prototype.resize = function () {
            if (!this.editContainer || !this.editHtml)
                return;
            PigPoorGame.Utils.mapTransformToHTML(this.editContainer, this.editHtml, this.app.renderer, {
                preferTextSize: true,
                targetTextSize: 24
            });
        };
        return LoginLayer;
    }());
    PigPoorGame.LoginLayer = LoginLayer;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var NetManager = /** @class */ (function () {
        function NetManager(config) {
            if (config === void 0) { config = undefined; }
            this.config = config;
            if (this.config && this.config.firebase) {
                firebase.initializeApp(this.config.firebase);
                var firebaseDB = firebase.firestore();
                firebaseDB.settings({
                    timestampsInSnapshots: true
                });
                this.collRef = firebaseDB.collection("leaderboard");
            }
        }
        NetManager.prototype.sendScore = function (prof, callback, count) {
            if (count === void 0) { count = 10; }
            if (!this.config || (!this.config.ajaxUrl && !this.config.firebase))
                return;
            if (this.config.ajaxUrl) {
                this._sendXHR(prof, callback);
                return;
            }
            if (this.collRef) {
                this.collRef
                    .orderBy("score", "desc")
                    .limit(count)
                    .get()
                    .then(function (data) {
                    var results = new Array();
                    if (!data.empty) {
                        data.docs.forEach(function (doc) {
                            results.push(doc.data());
                        });
                    }
                    callback(results);
                });
                if (prof) {
                    this.collRef.add({
                        name: prof.name,
                        gender: prof.gender,
                        score: prof.score,
                        roundDiration: prof.roundDiration
                    });
                }
            }
        };
        NetManager.prototype.getScores = function (callback, count) {
            if (count === void 0) { count = 10; }
            this.sendScore(undefined, callback, count);
        };
        NetManager.prototype._sendXHR = function (prof, callback) {
            var req = new XMLHttpRequest();
            req.setRequestHeader("Content-Type", "application/json");
            var data = undefined;
            if (prof) {
                data = new FormData();
                data.append("name", prof.name);
                data.append("isMan", "" + !!prof.isMen);
                data.append("score", "" + prof.score);
                req.open("POST", this.config.ajaxUrl, true);
            }
            else {
                req.open("GET", this.config.ajaxUrl, true);
            }
            req.onload = function (ev) {
                if (req.status == 200) {
                    callback(JSON.parse(req.responseText));
                    return;
                }
                callback(undefined);
            };
            req.send(data);
        };
        return NetManager;
    }());
    PigPoorGame.NetManager = NetManager;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var ObjectPool = /** @class */ (function () {
        function ObjectPool(buildFunc, preFunc, postFunc) {
            this.usedArr = [];
            this.freeArr = [];
            this.isFixed = false;
            this.buildFunc = buildFunc;
            this.preFunc = preFunc;
            this.postFunc = postFunc;
        }
        ObjectPool.prototype.populate = function (count, fixed) {
            if (count === void 0) { count = 20; }
            if (fixed === void 0) { fixed = false; }
            this.isFixed = fixed;
            this.freeArr = [];
            for (var i = 0; i < count; i++) {
                this.freeArr.push(this.buildFunc());
            }
        };
        ObjectPool.prototype.clear = function () {
            this.freeArr = undefined;
            this.usedArr = undefined;
        };
        Object.defineProperty(ObjectPool.prototype, "total", {
            get: function () {
                if (this.freeArr && this.usedArr)
                    return this.freeArr.length + this.usedArr.length;
                return 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ObjectPool.prototype, "free", {
            get: function () {
                if (!this.freeArr)
                    return 0;
                return this.freeArr.length;
            },
            enumerable: true,
            configurable: true
        });
        ObjectPool.prototype.pop = function () {
            var obj = this.freeArr.pop();
            if (obj) {
                if (!this.usedArr)
                    this.usedArr = [];
                this.usedArr.push(obj);
                this.preFunc(obj);
                return obj;
            }
            if (this.isFixed) {
                if (this.usedArr.length == 0) {
                    return undefined;
                }
                var last = this.usedArr.splice(0, 1)[0];
                this.postFunc(last);
                this.preFunc(last);
                this.usedArr.push(last);
                return last;
            }
            this.populate(1);
            return this.pop();
        };
        ObjectPool.prototype.push = function (obj) {
            if (!obj || !this.usedArr)
                return false;
            var index = this.usedArr.indexOf(obj);
            if (this.usedArr && index > -1) {
                this.postFunc(obj);
                this.usedArr.splice(index, 1);
                this.freeArr.push(obj);
                return true;
            }
            return false;
        };
        ObjectPool.prototype.pushAll = function () {
            if (!this.usedArr || !this.freeArr)
                return false;
            for (var i = this.usedArr.length - 1; i >= 0; i--) {
                this.push(this.usedArr[i]);
            }
            return true;
        };
        return ObjectPool;
    }());
    PigPoorGame.ObjectPool = ObjectPool;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var zLevelColors = [0xbaaad2, 0x8169a7, 0x522e8b, 0x551cae];
    var ParallaxLayer = /** @class */ (function () {
        function ParallaxLayer(app) {
            this.pigs = [];
            this.parallaxLayers = [];
            this.pigRefHeight = 120;
            this.spavnDuration = 3000;
            this._fader = 0;
            this.app = app;
            this.overlay = new PIXI.Sprite(PIXI.Texture.WHITE);
            this.overlay.tint = 0x0;
            this.overlay.alpha = 0.7;
        }
        ParallaxLayer.prototype.addToLoader = function (loader, callback) {
            var _this = this;
            loader.add("parallax", "./maps/parallax.json", function () {
                _this.stage = loader.resources["parallax"].stage;
            });
            return loader;
        };
        ParallaxLayer.prototype.init = function () {
            var _this = this;
            var prls = ["Front", "City_front", "City_back", "Clouds"];
            this.parallaxLayers = prls.map(function (name) {
                return _this.stage.getChildByPath(name);
            });
            var rect = this.stage.getBounds();
            this.overlay.x = rect.x;
            this.overlay.y = rect.y;
            this.overlay.width = rect.width;
            this.overlay.height = rect.height;
            this.stage.addChild(this.overlay);
            this.stage.y = 50;
            // pigs of war
            var pigssheet = this.app.loader.resources["anims"].spritesheet.animations;
            var pigs_anims = ["war_pig_1", "war_pig_2", "war_pig_3", "war_pig_4"];
            var pigs = pigs_anims.map(function (v) { return new PIXI.extras.AnimatedSprite(pigssheet[v]); });
            pigs.forEach(function (v) {
                var fh = v.textures[0].frame.height;
                v.scale.set(0.8);
                v.anchor.set(0.5);
                v.animationSpeed = 0.2;
                v.play();
                _this.pigs.push(new PigPoorGame.PigPoolElement(v));
            });
            this.app.stage.addChild(this.stage);
            this.spavn();
            setInterval(function () {
                _this.spavn();
            }, this.spavnDuration);
            this.app.ticker.add(function (dt) {
                _this.update(dt);
            });
            this.updateParallax(0, 0);
        };
        Object.defineProperty(ParallaxLayer.prototype, "fader", {
            get: function () {
                return this._fader;
            },
            set: function (f) {
                f = Math.min(1, Math.max(f, 0));
                this.stage.y = (1 - f) * 50;
                this.overlay.alpha = (1 - f) * 0.7;
                this._fader = f;
            },
            enumerable: true,
            configurable: true
        });
        ParallaxLayer.prototype.spavn = function () {
            var rect = this.stage.getBounds();
            var x = Math.random() > 0.5 ? rect.left : rect.right;
            var ZLevel = (Math.random() * 3) >> 0;
            var y = 100 + (150 + ZLevel * 80) * Math.random();
            var freePigs = this.pigs.filter(function (p) { return !p.isUsed; });
            if (freePigs.length == 0)
                return;
            var index = (Math.random() * freePigs.length) >> 0;
            var pig = freePigs[index];
            this.parallaxLayers[ZLevel].addChild(pig);
            pig.position.set(x, y);
            pig.direction = x < 0 ? 1 : -1;
            pig.zScale = 0.3 + (1 - ZLevel / 3.0) * 0.7;
            pig.isUsed = true;
            pig.tint = zLevelColors[ZLevel];
        };
        ParallaxLayer.prototype.release = function (pig) {
            if (!pig.isUsed)
                return;
            pig.parent.removeChild(pig);
            pig.isUsed = false;
        };
        ParallaxLayer.prototype.showed = function () { };
        ParallaxLayer.prototype.hidden = function () { };
        ParallaxLayer.prototype.fadeProgress = function (p, showing) { };
        ParallaxLayer.prototype.update = function (delta) {
            for (var _i = 0, _a = this.pigs; _i < _a.length; _i++) {
                var p = _a[_i];
                if (p.isUsed) {
                    p.update(delta);
                    if ((p.direction > 0 && p.position.x > this.app.renderer.width + 100) ||
                        (p.direction < 0 && p.position.x < -100)) {
                        this.release(p);
                    }
                }
            }
        };
        ParallaxLayer.prototype.updateParallax = function (x, y) {
            if (!this.stage)
                return;
            var npos = new PIXI.Point(x, y);
            npos.x /= this.app.renderer.width;
            npos.y /= this.app.renderer.height;
            for (var _i = 0, _a = this.parallaxLayers; _i < _a.length; _i++) {
                var layer = _a[_i];
                var factorX = layer.parallaxFactor;
                if (factorX == undefined)
                    factorX = 100;
                var factorY = layer.parallaxFactorY;
                if (factorY === undefined)
                    factorY = 100;
                layer.position.x = (0.5 - npos.x) * factorX;
                layer.position.y = (0.5 - npos.y) * factorY;
            }
        };
        return ParallaxLayer;
    }());
    PigPoorGame.ParallaxLayer = ParallaxLayer;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var PigPoolElement = /** @class */ (function (_super) {
        __extends(PigPoolElement, _super);
        function PigPoolElement(animatedSprite) {
            var _this = _super.call(this) || this;
            _this.zScale = 1;
            _this.tint = 0xffffff;
            _this.speed = 140;
            _this.defScale = 1;
            _this.direction = 0;
            _this.isUsed = false;
            _this.sprite = animatedSprite;
            _this.defScale = _this.sprite.scale.x;
            _this.addChild(animatedSprite);
            return _this;
        }
        PigPoolElement.prototype.update = function (dt) {
            this.position.x += (dt * this.direction * this.speed * this.zScale) / 60;
            this.scale.set(this.direction * this.defScale * this.zScale, this.defScale * this.zScale);
            this.sprite.tint = this.tint;
        };
        return PigPoolElement;
    }(PIXI.Container));
    PigPoorGame.PigPoolElement = PigPoolElement;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var Player = /** @class */ (function (_super) {
        __extends(Player, _super);
        function Player(run, jump, params) {
            var _this = _super.call(this) || this;
            _this._isJump = false;
            _this._dir = 0;
            _this._speed = 0;
            _this.jumpHeight = 200;
            _this.jumpDuration = 0.6;
            _this.safeArea = undefined;
            _this.startY = 0;
            _this.jumpTimer = 0;
            _this.playerRunAnimator = new PIXI.extras.AnimatedSprite(run, false);
            _this.playerJumpAnimator = new PIXI.extras.AnimatedSprite(jump, false);
            _this.addChild(_this.playerRunAnimator, _this.playerJumpAnimator);
            _this.playerRunAnimator.anchor.x = _this.playerJumpAnimator.anchor.x = 0.5;
            _this.playerRunAnimator.anchor.y = _this.playerJumpAnimator.anchor.y = 1;
            //this.playerJumpAnimator.x = this.playerRunAnimator.x = this.playerRunAnimator.width * 0.5;
            _this.playerJumpAnimator.play();
            _this.playerRunAnimator.play();
            _this.speed = 300;
            _this.jumping = false;
            _this.direction = 0;
            if (params) {
                _this.speed = params.speed;
                _this.jumpDuration = params.jumpDuration;
            }
            return _this;
        }
        Object.defineProperty(Player.prototype, "speed", {
            get: function () {
                return this._speed;
            },
            set: function (s) {
                var h = this.playerRunAnimator.textures[0].frame.width;
                var rate = s / (h * this.playerRunAnimator.totalFrames);
                this.playerRunAnimator.animationSpeed = rate;
                this._speed = s;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Player.prototype, "headPoint", {
            get: function () {
                var point = this.position.clone();
                point.y -= (192 - 23);
                return point;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Player.prototype, "jumping", {
            get: function () {
                return this._isJump;
            },
            set: function (j) {
                this.playerRunAnimator.visible = !j;
                this.playerJumpAnimator.visible = j;
                if (j && !this._isJump) {
                    this.startY = this.position.y;
                    this.jumpTimer = 0; //up
                }
                this._isJump = j;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Player.prototype, "direction", {
            get: function () {
                return this._dir;
            },
            set: function (direction) {
                //if( this._isJump) return;
                if (direction > 0) {
                    this.playerJumpAnimator.scale.x = this.playerRunAnimator.scale.x = -1;
                }
                else if (direction < 0) {
                    this.playerJumpAnimator.scale.x = this.playerRunAnimator.scale.x = 1;
                }
                if (!this._isJump) {
                    if (direction != this._dir) {
                        //if(direction != 0)
                        // this.playerRun.play();
                        //else
                        //    this.playerRun.gotoAndStop(1);
                    }
                }
                this._dir = direction;
            },
            enumerable: true,
            configurable: true
        });
        Player.prototype.jump = function () {
            if (this._isJump)
                return;
            this.jumping = true;
        };
        Player.prototype.update = function (dt) {
            this.position.x += (this.direction * dt * this.speed) / 60;
            if (this.safeArea) {
                var _x = this.x;
                if (_x > this.safeArea.right && this.direction > 0 ||
                    _x < this.safeArea.left && this.direction < 0) {
                    this.x = Math.max(this.safeArea.left, Math.min(this.x, this.safeArea.right));
                }
            }
            if (this.direction != 0) {
                this.playerRunAnimator.update(dt);
            }
            if (this.jumping) {
                this.evaluateJumping(dt);
            }
        };
        Player.prototype.evaluateJumping = function (dt) {
            var tick = dt / 60;
            var delta = tick / this.jumpDuration;
            this.jumpTimer += delta;
            var normalT = 2 * (0.5 - this.jumpTimer);
            var parabl = (1 - normalT * normalT) * this.jumpHeight;
            this.position.y = this.startY - parabl;
            //this.position.y = this.startY - Math.sin(this.jumpTimer * Math.PI) * this.jumpHeight;
            this.playerJumpAnimator.gotoAndStop(Math.round(this.jumpTimer * this.playerJumpAnimator.totalFrames));
            if (this.jumpTimer > 1) {
                this.position.y = this.startY;
                this.jumping = false;
            }
        };
        return Player;
    }(PIXI.Container));
    PigPoorGame.Player = Player;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var ResultLayer = /** @class */ (function () {
        function ResultLayer(app) {
            this.wasInited = false;
            this.app = app;
            this.ratingHtml = document.querySelector("#result-table");
        }
        ResultLayer.prototype.addToLoader = function (loader, callback) {
            var _this = this;
            loader.add("result", "./maps/result.json", function () {
                if (callback)
                    callback();
                _this.stage = loader.resources["result"].stage;
            });
            return loader;
        };
        ResultLayer.prototype.init = function () {
            var _this = this;
            var preset = {
                hover: 0xc2adc6,
                disabled: 0x5b61b3,
                click: 0x865e84
            };
            if (this.wasInited)
                return;
            var playAgainButton = this.stage.getChildByPath("Result/PlayAgainButton");
            PigPoorGame.Utils.registerAsButton(playAgainButton, preset, function () {
                _this.app.activateLayer(_this.app.layers["game"]);
            });
            var playAgainButtonRaiting = this.stage.getChildByPath("Board/PlayAgainButton");
            PigPoorGame.Utils.registerAsButton(playAgainButtonRaiting, preset, function () {
                _this.app.activateLayer(_this.app.layers["game"]);
            });
            var ratingButton = this.stage.getChildByPath("Result/RatingButton");
            PigPoorGame.Utils.registerAsButton(ratingButton, preset, function () {
                _this.showResultBoard();
            });
            this.totalScoreText = this.stage.getChildByPath("Result/TotalScore").text;
            this.updateText(0);
            this.ratingContainer = this.stage.getChildByPath("Board/RaitingBox");
            this.ratingContainer.visible = false;
            this.wasInited = true;
            window.addEventListener("resize", function () {
                PigPoorGame.Utils.mapTransformToHTML(_this.ratingContainer, _this.ratingHtml, _this.app.renderer, {
                    preferTextSize: true,
                    targetTextSize: 22
                });
            });
        };
        ResultLayer.prototype.showResultBoard = function () {
            this.stage.getChildByPath("Result").visible = false;
            this.stage.getChildByPath("Board").visible = true;
            PigPoorGame.Utils.mapTransformToHTML(this.ratingContainer, this.ratingHtml, this.app.renderer, {
                preferTextSize: true,
                targetTextSize: 22
            });
            this.ratingHtml.style.display = "block";
            //this.ratingHtml!.scrollTo(0, 1);
        };
        ResultLayer.prototype.hideReslutBoard = function () {
            this.stage.getChildByPath("Result").visible = true;
            this.stage.getChildByPath("Board").visible = false;
            this.ratingHtml.style.display = "none";
        };
        ResultLayer.prototype.fadeProgress = function (p, showing) {
            if (!showing)
                return;
            var parallax = this.app.layers["parallax"];
            parallax.fader = 1 - p;
        };
        ResultLayer.prototype.beforeShowed = function () {
            var _this = this;
            var menI = this.stage.getChildByPath("Result/PlayerM");
            var womenI = this.stage.getChildByPath("Result/PlayerW");
            menI.visible = this.app.userConfig.gender == PigPoorGame.UserGender.BOY;
            womenI.visible = this.app.userConfig.gender == PigPoorGame.UserGender.GIRL;
            if (this.app.userConfig.score > 0) {
                this.app.netManager.sendScore(this.app.userConfig, function (resp) {
                    _this.populateBoard(resp);
                });
            }
            else {
                this.app.netManager.getScores(function (resp) {
                    _this.populateBoard(resp);
                });
            }
            var ga = this.app.gameConfig.greetings;
            if (ga && ga.length > 0) {
                var text = ga[(Math.random() * ga.length) >> 0];
                var view = this.stage.getChildByPath("Result/GreetingsText").text;
                view.text = this.app.userConfig.formatText(text);
            }
        };
        ResultLayer.prototype.populateBoard = function (resp) {
            this.ratingHtml.innerHTML = "";
            var index = 1;
            if (!resp || (resp && resp.length == 0)) {
                resp = [this.app.userConfig];
            }
            var count = Math.max(5, resp.length);
            for (var i = 0; i < count; i++) {
                var u = resp[i];
                if (u) {
                    this.ratingHtml.appendChild(this.genBlock(i + 1, u.name, u.score));
                }
                else {
                    this.ratingHtml.appendChild(document.createElement("div"));
                }
            }
            if (this.ratingHtml.scrollTo)
                this.ratingHtml.scrollTo(0, 1);
        };
        ResultLayer.prototype.genBlock = function (id, name, score) {
            var node = document.createElement("div");
            node.innerHTML = "<span id=\"score-id\">" + id + "</span> <span id=\"score-name\">  " + name + "</span> <span id=\"score-text\">" + score + "</span>";
            return node;
        };
        ResultLayer.prototype.showed = function () {
            var _this = this;
            var proxy = {
                val: 0
            };
            this.app.soundManager.Play("counter");
            gsap.TweenLite.to(proxy, 1, {
                val: this.app.userConfig.score,
                onUpdate: function () {
                    _this.updateText(proxy.val >> 0);
                }
            }).play();
        };
        ResultLayer.prototype.updateText = function (score) {
            this.totalScoreText.text = PigPoorGame.Utils.StringPad(score, 4);
        };
        ResultLayer.prototype.hidden = function () {
            this.hideReslutBoard();
        };
        ResultLayer.prototype.update = function (delta) { };
        return ResultLayer;
    }());
    PigPoorGame.ResultLayer = ResultLayer;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var SoundManager = /** @class */ (function () {
        function SoundManager(manifest) {
            this.sounds = {};
            for (var _i = 0, manifest_1 = manifest; _i < manifest_1.length; _i++) {
                var etry = manifest_1[_i];
                var sound = new Howl({
                    src: etry.src,
                    preload: etry.preload,
                });
                this.sounds[etry.name] = sound;
            }
        }
        SoundManager.prototype.Play = function (name, loop, volume, fadeIn) {
            if (loop === void 0) { loop = false; }
            if (volume === void 0) { volume = 1; }
            if (fadeIn === void 0) { fadeIn = false; }
            var s = this.sounds[name];
            if (s) {
                s.loop(loop);
                s.play();
                if (fadeIn) {
                    s.fade(0, volume, 1);
                }
                else {
                    s.volume(volume);
                }
            }
            return s;
        };
        SoundManager.prototype.Stop = function (name, fadeout) {
            var _this = this;
            if (fadeout === void 0) { fadeout = false; }
            if (this.sounds[name]) {
                if (!fadeout) {
                    this.sounds[name].stop();
                }
                else {
                    var v = this.sounds[name].volume();
                    this.sounds[name].fade(v, 0, 1);
                    setTimeout(function () {
                        _this.sounds[name].stop();
                    }, 500);
                }
            }
        };
        return SoundManager;
    }());
    PigPoorGame.SoundManager = SoundManager;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var UserGender;
    (function (UserGender) {
        UserGender[UserGender["UFO"] = 0] = "UFO";
        UserGender[UserGender["BOY"] = 1] = "BOY";
        UserGender[UserGender["GIRL"] = 2] = "GIRL";
    })(UserGender = PigPoorGame.UserGender || (PigPoorGame.UserGender = {}));
    var User = /** @class */ (function () {
        function User() {
            this._name = "";
            this._gender = UserGender.UFO;
            this.roundDiration = 0;
            this.totalCoints = 0;
            this.totalShits = 0;
            this.score = 0;
            this._uuid = "";
            this._uuid = PIXI.utils.uid().toString(16);
        }
        User.prototype.load = function () {
            if (Cookies) {
                var conf = Cookies.getJSON("UserConfig");
                if (conf) {
                    this._gender = conf.gender || UserGender.UFO;
                    this._name = conf.name;
                }
            }
        };
        User.prototype.save = function () {
            if (Cookies) {
                Cookies.set("UserConfig", {
                    gender: this._gender,
                    name: this.name,
                });
            }
        };
        Object.defineProperty(User.prototype, "name", {
            get: function () {
                return this._name;
            },
            set: function (n) {
                if (name == n || n == undefined || n.length < 2)
                    return;
                this._name = n;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(User.prototype, "gender", {
            get: function () {
                return this._gender;
            },
            set: function (g) {
                if (g == this._gender)
                    return;
                this._gender = g;
            },
            enumerable: true,
            configurable: true
        });
        User.prototype.resetScore = function () {
            this.score = 0;
            this.totalCoints = 0;
            this.totalShits = 0;
            this.roundDiration = 0;
        };
        User.prototype.formatText = function (text) {
            text = text.replace(/\${name}/g, this.name);
            text = text.replace(/\${score}/g, "" + this.score);
            text = text.replace(/\${coints}/g, "" + this.totalCoints);
            text = text.replace(/\${shits}/g, "" + this.totalShits);
            var reg = undefined;
            if (this.gender == UserGender.BOY) {
                text = text.replace(/(?:\$if_girl{)(.*?)(?:})/g, "");
                reg = new RegExp(/(?:\$if_boy{)(.*?)(?:})/i);
            }
            else {
                text = text.replace(/(?:\$if_boy{)(.*?)(?:})/g, "");
                reg = new RegExp(/(?:\$if_girl{)(.*?)(?:})/i);
            }
            var math = undefined;
            do {
                math = reg.exec(text);
                if (math) {
                    text = text.replace(math[0], math[1]);
                }
            } while (math);
            return text;
        };
        return User;
    }());
    PigPoorGame.User = User;
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var Utils;
    (function (Utils) {
        function registerAsButton(button, props, click) {
            var diffTint = button.tint;
            if (props.hover) {
                button.on("pointerover", function () {
                    button.tint = props.hover;
                });
                button.on("pointerout", function () {
                    button.tint = diffTint;
                });
            }
            if (props.disabled && !button.interactive) {
                button.tint = props.disabled;
            }
            button.on("pointerdown", function () {
                if (props.click)
                    button.tint = props.click;
            });
            button.on("pointerup", function () {
                if (click)
                    click();
            });
        }
        Utils.registerAsButton = registerAsButton;
        function shakingElement(target, duration, update) {
            if (duration === void 0) { duration = 1; }
            var proxy = {
                timer: 0
            };
            var amplitude = 10;
            var loops = 3;
            var start = target.position.x;
            gsap.TweenLite.to(proxy, duration, {
                onUpdate: function () {
                    target.position.x = start + Math.sin(Math.PI * 2 * loops * proxy.timer) * amplitude;
                    if (update)
                        update();
                },
                timer: 1
            }).play();
        }
        Utils.shakingElement = shakingElement;
        function mapTransformToHTML(from, to, renderer, options) {
            var props = options || {};
            if (!from || !to || !renderer)
                return;
            var rect = from.getBounds();
            var domRect = renderer.view.getBoundingClientRect();
            var parentDomRect = to.parentElement.getBoundingClientRect();
            var offsetX = domRect.left - parentDomRect.left;
            var offsetY = domRect.top - parentDomRect.top;
            var scale = renderer.width / domRect.width;
            //console.log(scale);
            rect.x /= renderer.width;
            rect.y /= renderer.height;
            rect.width /= renderer.width;
            rect.height /= renderer.height;
            if (props.preferTextSize)
                to.style.fontSize = (props.targetTextSize || 24) / scale + "px";
            to.style.position = "absolute";
            to.style.left = rect.x * domRect.width + offsetX + "px";
            to.style.top = rect.y * domRect.height + offsetY + "px";
            to.style.width = rect.width * domRect.width + "px";
            to.style.height = rect.height * domRect.height + "px";
        }
        Utils.mapTransformToHTML = mapTransformToHTML;
        function StringPad(num, size) {
            var s = num + "";
            while (s.length < size)
                s = "0" + s;
            return s;
        }
        Utils.StringPad = StringPad;
        /**
         * Converts an RGB color value to HSV. Conversion formula
         * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
         * Assumes r, g, and b are contained in the set [0, 1] and
         * returns h, s, and v in the set [0, 1].
         *
         * @param   Number  r       The red color value
         * @param   Number  g       The green color value
         * @param   Number  b       The blue color value
         * @return  Array           The HSV representation
         */
        function rgbToHsv(r, g, b) {
            //r /= 255, g /= 255, b /= 255;
            var max = Math.max(r, g, b), min = Math.min(r, g, b);
            var h = 0, s = 0, v = max;
            var d = max - min;
            s = max == 0 ? 0 : d / max;
            if (max == min) {
                h = 0; // achromatic
            }
            else {
                switch (max) {
                    case r:
                        h = (g - b) / d + (g < b ? 6 : 0);
                        break;
                    case g:
                        h = (b - r) / d + 2;
                        break;
                    case b:
                        h = (r - g) / d + 4;
                        break;
                }
                h /= 6;
            }
            return [h, s, v];
        }
        Utils.rgbToHsv = rgbToHsv;
        /**
         * Converts an HSV color value to RGB. Conversion formula
         * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
         * Assumes h, s, and v are contained in the set [0, 1] and
         * returns r, g, and b in the set [0, 1].
         *
         * @param   Number  h       The hue
         * @param   Number  s       The saturation
         * @param   Number  v       The value
         * @return  Array           The RGB representation
         */
        function hsvToRgb(h, s, v) {
            var r = 0, g = 0, b = 0;
            var i = Math.floor(h * 6);
            var f = h * 6 - i;
            var p = v * (1 - s);
            var q = v * (1 - f * s);
            var t = v * (1 - (1 - f) * s);
            switch (i % 6) {
                case 0:
                    (r = v), (g = t), (b = p);
                    break;
                case 1:
                    (r = q), (g = v), (b = p);
                    break;
                case 2:
                    (r = p), (g = v), (b = t);
                    break;
                case 3:
                    (r = p), (g = q), (b = v);
                    break;
                case 4:
                    (r = t), (g = p), (b = v);
                    break;
                case 5:
                    (r = v), (g = p), (b = q);
                    break;
            }
            return [r, g, b];
        }
        Utils.hsvToRgb = hsvToRgb;
    })(Utils = PigPoorGame.Utils || (PigPoorGame.Utils = {}));
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var Objects;
    (function (Objects) {
        var CoinsObject = /** @class */ (function (_super) {
            __extends(CoinsObject, _super);
            function CoinsObject(layer) {
                var _this = _super.call(this) || this;
                _this.shitTextPool = [];
                _this.speeds = new PIXI.Point(0, 0);
                _this.isShowed = false;
                _this.layer = layer;
                var coins = layer.app.loader.resources["anims"].spritesheet.animations["coin"];
                _this.anim = new PIXI.extras.AnimatedSprite(coins);
                _this.anim.animationSpeed = 0.2;
                _this.anim.anchor.set(0.5);
                _this.addChild(_this.anim);
                return _this;
                //this.setText("NDFL 20");
            }
            CoinsObject.prototype.update = function (dt) {
                this.speeds.y += dt * 100 / 60;
                this.position.x += dt * this.speeds.x / 60;
                this.position.y += dt * this.speeds.y / 60;
            };
            return CoinsObject;
        }(PIXI.Container));
        Objects.CoinsObject = CoinsObject;
    })(Objects = PigPoorGame.Objects || (PigPoorGame.Objects = {}));
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var Objects;
    (function (Objects) {
        var ParticleBuilder = /** @class */ (function () {
            function ParticleBuilder(layer) {
                this.shitProbability = 0.3;
                this.duration = 0;
                this._poolGenTimer = -1;
                this.layer = layer;
                this.player = layer.player;
                this.shitProbability = layer.app.gameConfig.shitProbability || 0.3;
                this.pig = layer.bigPig;
                this.roadLayer = layer.stage.getChildByPath("RoadLayer");
                this.coinLayer = layer.stage.getChildByPath("Coins");
                this.road = this.roadLayer.getChildByPath("Road");
                this.shitPool = new PigPoorGame.ObjectPool(this.buildShit.bind(this), this.preGet.bind(this), this.posSet.bind(this));
                var count = layer.app.gameConfig.shitsPerSceen || 7;
                this.shitPool.populate(count, true);
                this.coinPool = new PigPoorGame.ObjectPool(this.buildCoins.bind(this), this.preGet.bind(this), this.posSet.bind(this));
            }
            ParticleBuilder.prototype.buildShit = function () {
                var shit = new Objects.ShitObject(this.layer, this.layer.app.gameConfig.shitLabels);
                shit.visible = false;
                this.roadLayer.addChild(shit);
                return shit;
            };
            ParticleBuilder.prototype.buildCoins = function () {
                var coin = new Objects.CoinsObject(this.layer);
                coin.visible = false;
                this.coinLayer.addChild(coin);
                return coin;
            };
            ParticleBuilder.prototype.preGet = function (v) {
                v.position.copy(this.pig.position);
                v.position.y -= this.pig.height * 0.5;
                v.visible = true;
                v.alpha = 1;
                if (v instanceof Objects.CoinsObject) {
                    var pos = (v.anim.totalFrames * Math.random()) >> 0;
                    v.anim.gotoAndPlay(pos);
                }
            };
            ParticleBuilder.prototype.posSet = function (v) {
                v.visible = false;
                v.speeds.set(0, 0);
                if (v instanceof Objects.ShitObject) {
                    v.hideText();
                    clearTimeout(v.timeoutHandler);
                }
                if (v instanceof Objects.CoinsObject) {
                    v.anim.stop();
                }
            };
            ParticleBuilder.prototype.start = function (duration) {
                var _this = this;
                if (duration === void 0) { duration = 100; }
                this.duration = duration;
                this.pause();
                this._poolGenTimer = setInterval(function () {
                    _this.gen();
                }, duration);
            };
            ParticleBuilder.prototype.pause = function () {
                clearInterval(this._poolGenTimer);
            };
            ParticleBuilder.prototype.reset = function () {
                this.shitPool.pushAll();
                this.coinPool.pushAll();
                this.layer.app.soundManager.Stop("shit_spanv");
                this.layer.app.soundManager.Stop("coin_spanv");
                this.pause();
            };
            ParticleBuilder.prototype.gen = function (boost) {
                if (boost === void 0) { boost = 1; }
                var rnd = Math.random();
                var shitBlinkDuration = 0.5;
                var obj = rnd < this.shitProbability ? this.shitPool.pop() : this.coinPool.pop();
                if (obj instanceof Objects.ShitObject) {
                    this.layer.app.soundManager.Play("shit_spanv");
                    var _pool_1 = this.shitPool;
                    var liveTime = (this.layer.app.gameConfig.shitsLiveTime | 5);
                    var timeout = liveTime - shitBlinkDuration;
                    timeout += (0.5 - Math.random()) * 0.2 * liveTime;
                    var shit_1 = obj;
                    shit_1.timeoutHandler = setTimeout(function () {
                        shit_1.blink(shitBlinkDuration, function () {
                            _pool_1.push(shit_1);
                        });
                    }, timeout * 1000);
                }
                else {
                    this.layer.app.soundManager.Play("coin_spanv");
                }
                obj.speeds.x = -this.pig.direction * (20 + 50 * Math.random()) * boost;
                obj.speeds.y = -(30 + Math.random() * 100) * boost;
            };
            ParticleBuilder.prototype.brust = function (count) {
                while (count > 0) {
                    this.gen(2);
                    count--;
                }
            };
            ParticleBuilder.prototype.update = function (dt) {
                this.updateShits(dt);
                this.updateCoins(dt);
            };
            ParticleBuilder.prototype.updateShits = function (dt) {
                if (!this.shitPool.usedArr)
                    return;
                var safeY = this.road.position.y - this.road.height + 10;
                var arr = this.shitPool.usedArr;
                for (var i = arr.length - 1; i >= 0; --i) {
                    var obj = arr[i];
                    var hit = this.hitTest(obj.hitObject, this.player.hitObject);
                    if (hit) {
                        this.shitPool.push(obj);
                        this.layer.shitHitEvent();
                        this.layer.app.soundManager.Play("shit_catch");
                        continue;
                    }
                    if (obj.position.y < safeY) {
                        obj.update(dt);
                    }
                    else {
                        obj.showText();
                    }
                }
            };
            ParticleBuilder.prototype.updateCoins = function (dt) {
                if (!this.coinPool.usedArr)
                    return;
                var safeY = this.layer.app.renderer.height + 20;
                var arr = this.coinPool.usedArr;
                for (var i = arr.length - 1; i >= 0; --i) {
                    var obj = arr[i];
                    var hit = this.hitTest(obj, this.player.hitObject);
                    if (hit) {
                        this.layer.coinHitEvent();
                        this.layer.app.soundManager.Play("coin_catch");
                    }
                    if (obj.position.y < safeY && !hit) {
                        obj.update(dt);
                    }
                    else {
                        this.coinPool.push(obj);
                    }
                }
            };
            ParticleBuilder.prototype.hitTest = function (objA, objB) {
                var rectA = objA.getBounds();
                var rectB = objB.getBounds();
                if (rectA.left > rectB.right)
                    return false;
                if (rectA.right < rectB.left)
                    return false;
                if (rectA.bottom < rectB.top)
                    return false;
                if (rectA.top > rectB.bottom)
                    return false;
                return true;
            };
            return ParticleBuilder;
        }());
        Objects.ParticleBuilder = ParticleBuilder;
    })(Objects = PigPoorGame.Objects || (PigPoorGame.Objects = {}));
})(PigPoorGame || (PigPoorGame = {}));
var PigPoorGame;
(function (PigPoorGame) {
    var Objects;
    (function (Objects) {
        Objects.SHIT_TEXT_OFFSET = 15;
        var ShitObject = /** @class */ (function (_super) {
            __extends(ShitObject, _super);
            function ShitObject(layer, text) {
                var _this = _super.call(this) || this;
                _this.shitTextPool = [];
                _this.speeds = new PIXI.Point(0, 0);
                _this.isShowed = false;
                _this.timeoutHandler = -1;
                _this.shitTextPool = text || ["NALOG"];
                var loader = layer.app.loader;
                _this.layer = layer;
                var shit = loader.filter(function (v) {
                    return v.name.indexOf("shit") > -1;
                })[0].texture;
                var enter_name = loader.filter(function (v) {
                    return v.name.indexOf("enter_name") > -1;
                })[0].texture;
                _this.sprite = new PIXI.Sprite(shit);
                _this.sprite.anchor.set(0.5, 1);
                _this.textBg = new PIXI.Sprite(enter_name);
                _this.textBg.anchor.set(0.5, 0.5);
                _this.text = new PIXI.Text("NALOG", {
                    fontFamily: "Pixel Cyr, Arial",
                    fontSize: 14
                });
                _this.text.anchor.set(0.5, 0.5);
                _this.addChild(_this.textBg);
                _this.addChild(_this.text);
                _this.addChild(_this.sprite);
                var hitRect = _this.sprite.getLocalBounds();
                hitRect.height -= 15;
                hitRect.y += 15;
                _this.hitObject = new PIXI.Graphics();
                _this.hitObject.lineStyle(2, 0x0000ff);
                _this.hitObject.drawShape(hitRect);
                _this.hitObject.visible = PigPoorGame.DEBUG;
                _this.addChild(_this.hitObject);
                _this.text.visible = false;
                _this.textBg.visible = false;
                return _this;
                //this.setText("NDFL 20");
            }
            ShitObject.prototype.setText = function (text) {
                this.text.text = text;
                this.textBg.width = Math.max(this.text.width + 16, this.sprite.width);
                this.textBg.height = this.text.height + 8;
                this.textBg.x = this.text.x = 0;
                this.textBg.y = this.text.y = -(this.sprite.height + Objects.SHIT_TEXT_OFFSET);
            };
            ShitObject.prototype.showText = function () {
                if (this.isShowed)
                    return;
                this.isShowed = true;
                var id = (this.shitTextPool.length * Math.random()) >> 0;
                this.setText(this.layer.app.userConfig.formatText(this.shitTextPool[id]));
                this.text.visible = this.textBg.visible = true;
            };
            ShitObject.prototype.hideText = function () {
                this.isShowed = false;
                this.text.visible = this.textBg.visible = false;
            };
            ShitObject.prototype.blink = function (duration, endCallback) {
                var _this = this;
                if (duration === void 0) { duration = 3; }
                var blink_times = 5;
                var proxy = {
                    val: this.alpha
                };
                gsap.TweenLite.to(proxy, duration, {
                    val: 0,
                    onUpdate: function () {
                        _this.alpha = 0.5 + 0.5 * Math.sin(Math.PI * proxy.val * blink_times);
                    },
                    onComplete: function () {
                        if (endCallback)
                            endCallback();
                    }
                }).play();
            };
            ShitObject.prototype.update = function (dt) {
                this.speeds.y += dt * 100 / 60;
                this.position.x += dt * this.speeds.x / 60;
                this.position.y += dt * this.speeds.y / 60;
            };
            return ShitObject;
        }(PIXI.Container));
        Objects.ShitObject = ShitObject;
    })(Objects = PigPoorGame.Objects || (PigPoorGame.Objects = {}));
})(PigPoorGame || (PigPoorGame = {}));
var TiledOG;
(function (TiledOG) {
    var ContainerBuilder;
    (function (ContainerBuilder) {
        function ApplyMeta(meta, target) {
            target.name = meta.name;
            target.tiledId = meta.id;
            target.width = meta.width || target.width;
            target.height = meta.height || target.height;
            target.rotation = ((meta.rotation || 0) * Math.PI) / 180.0;
            if (meta.x)
                target.x = meta.x;
            if (meta.y)
                target.y = meta.y;
            target.visible = meta.visible == undefined ? true : meta.visible;
            target.types = meta.type ? meta.type.split(":") : [];
            var type = Tiled.Utils.Objectype(meta);
            target.primitive = TiledOG.Primitives.BuildPrimitive(meta);
            if (meta.properties) {
                target.alpha = meta.properties.opacity || 1;
                Object.assign(target, meta.properties);
            }
            if (TiledOG.Config.debugContainers) {
                setTimeout(function () {
                    var rect = new PIXI.Graphics();
                    rect.lineStyle(2, 0xff0000, 0.7)
                        .drawRect(target.x, target.y, meta.width, meta.height)
                        .endFill();
                    if (target instanceof PIXI.Sprite) {
                        rect.y -= target.height;
                    }
                    target.parent.addChild(rect);
                }, 30);
            }
        }
        ContainerBuilder.ApplyMeta = ApplyMeta;
        function Build(meta) {
            var types = meta.type ? meta.type.split(":") : [];
            var container = undefined; // new TiledOG.TiledContainer();
            if (types.indexOf("mask") > -1) {
                container = new PIXI.Sprite(PIXI.Texture.WHITE);
            }
            else {
                container = new TiledOG.TiledContainer();
            }
            if (meta.gid) {
                if (container instanceof PIXI.Sprite) {
                    container.anchor = TiledOG.Config.defSpriteAnchor;
                }
                else {
                    container.pivot = TiledOG.Config.defSpriteAnchor;
                    container.hitArea = new PIXI.Rectangle(0, 0, meta.width, meta.height);
                }
            }
            ApplyMeta(meta, container);
            return container;
        }
        ContainerBuilder.Build = Build;
    })(ContainerBuilder = TiledOG.ContainerBuilder || (TiledOG.ContainerBuilder = {}));
})(TiledOG || (TiledOG = {}));
var TiledOG;
(function (TiledOG) {
    var SpriteBuilder;
    (function (SpriteBuilder) {
        function сreateSprite(meta) {
            // TODO make load from texture atlass
            var sprite = new PIXI.Sprite();
            //TODO Set anchor and offsets to center (.5, .5)
            if (!meta.fromImageLayer) {
                sprite.anchor = TiledOG.Config.defSpriteAnchor;
            }
            //debugger
            TiledOG.ContainerBuilder.ApplyMeta(meta, sprite);
            var obj = meta.img.objectgroup;
            if (obj) {
                sprite.primitive = TiledOG.Primitives.BuildPrimitive(obj.objects[0]);
            }
            var hFlip = meta.properties.hFlip;
            var vFlip = meta.properties.vFlip;
            if (hFlip) {
                sprite.scale.x *= -1;
                sprite.anchor.x = 1;
            }
            if (vFlip) {
                sprite.scale.y *= -1;
                sprite.anchor.y = 0;
            }
            return sprite;
        }
        function Build(meta) {
            //debugger
            var sprite = сreateSprite(meta);
            return sprite;
        }
        SpriteBuilder.Build = Build;
    })(SpriteBuilder = TiledOG.SpriteBuilder || (TiledOG.SpriteBuilder = {}));
})(TiledOG || (TiledOG = {}));
var TiledOG;
(function (TiledOG) {
    var TextBuilder;
    (function (TextBuilder) {
        function roundAlpha(canvas) {
            var ctx = canvas.getContext("2d");
            var data = ctx.getImageData(0, 0, canvas.width, canvas.height);
            for (var i = 3; i < data.data.length; i += 4) {
                data.data[i] = data.data[i] > 200 ? 255 : 0;
            }
            ctx.putImageData(data, 0, 0);
        }
        function createText(meta) {
            var container = new TiledOG.TiledContainer();
            var pixiText = new PIXI.Text(meta.text.text, {
                wordWrap: meta.text.wrap,
                wordWrapWidth: meta.width,
                fill: Tiled.Utils.HexStringToHexInt(meta.text.color) || 0x000000,
                align: meta.text.valign || "center",
                fontFamily: meta.text.fontfamily || "Arial",
                fontWeight: meta.text.bold ? "bold" : "normal",
                fontStyle: meta.text.italic ? "italic" : "normal",
                fontSize: meta.text.pixelsize || "16px"
            });
            pixiText.name = meta.name + "_Text";
            if (TiledOG.Config.roundFontAlpha) {
                pixiText.texture.once("update", function (x) {
                    roundAlpha(pixiText.canvas);
                    pixiText.texture.baseTexture.update();
                    console.log("update");
                });
            }
            var props = meta.properties;
            meta.properties = {};
            TiledOG.ContainerBuilder.ApplyMeta(meta, container);
            container.pivot.set(0, 0);
            switch (meta.text.halign) {
                case "right":
                    {
                        pixiText.anchor.x = 1;
                        pixiText.position.x = meta.width;
                    }
                    break;
                case "center":
                    {
                        pixiText.anchor.x = 0.5;
                        pixiText.position.x = meta.width * 0.5;
                    }
                    break;
                default:
                    {
                        pixiText.anchor.x = 0;
                        pixiText.position.x = 0;
                    }
                    break;
            }
            switch (meta.text.valign) {
                case "bottom":
                    {
                        pixiText.anchor.y = 1;
                        pixiText.position.y = meta.height;
                    }
                    break;
                case "center":
                    {
                        pixiText.anchor.y = 0.5;
                        pixiText.position.y = meta.height * 0.5;
                    }
                    break;
                default:
                    {
                        pixiText.anchor.y = 0;
                        pixiText.position.y = 0;
                    }
                    break;
            }
            if (props) {
                pixiText.style.stroke = Tiled.Utils.HexStringToHexInt(meta.properties.strokeColor) || 0;
                pixiText.style.strokeThickness = meta.properties.strokeThickness || 0;
                pixiText.style.padding = meta.properties.fontPadding || 0;
                Object.assign(pixiText, props);
            }
            //_cont.parentGroup = _layer.group;
            container.addChild(pixiText);
            container.text = pixiText;
            return container;
        }
        function Build(meta) {
            return createText(meta);
        }
        TextBuilder.Build = Build;
    })(TextBuilder = TiledOG.TextBuilder || (TiledOG.TextBuilder = {}));
})(TiledOG || (TiledOG = {}));
var Tiled;
(function (Tiled) {
    var MultiSpritesheet = /** @class */ (function () {
        function MultiSpritesheet(sheets) {
            var _this = this;
            this.sheets = [];
            this.images = {};
            if (sheets) {
                sheets.forEach(function (element) {
                    _this.add(element);
                });
            }
        }
        MultiSpritesheet.prototype.add = function (sheet) {
            if (!sheet)
                throw "Sheet can't be undefined";
            this.sheets.push(sheet);
        };
        MultiSpritesheet.prototype.addTexture = function (tex, id) {
            this.images[id] = tex;
        };
        Object.defineProperty(MultiSpritesheet.prototype, "textures", {
            get: function () {
                var map = {};
                for (var _i = 0, _a = this.sheets; _i < _a.length; _i++) {
                    var spr = _a[_i];
                    Object.assign(map, spr.textures);
                }
                Object.assign(map, this.images);
                return map;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MultiSpritesheet.prototype, "animations", {
            get: function () {
                var map = {};
                for (var _i = 0, _a = this.sheets; _i < _a.length; _i++) {
                    var spr = _a[_i];
                    Object.assign(map, spr.animations);
                }
                return map;
            },
            enumerable: true,
            configurable: true
        });
        return MultiSpritesheet;
    }());
    Tiled.MultiSpritesheet = MultiSpritesheet;
})(Tiled || (Tiled = {}));
var TiledOG;
(function (TiledOG) {
    var TiledContainer = /** @class */ (function (_super) {
        __extends(TiledContainer, _super);
        function TiledContainer() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.layerHeight = 0;
            _this.layerWidth = 0;
            return _this;
        }
        return TiledContainer;
    }(PIXI.Container));
    TiledOG.TiledContainer = TiledContainer;
})(TiledOG || (TiledOG = {}));
/// <reference path ="../../node_modules/pixi-layers/dist/pixi-layers.d.ts">
var TiledOG;
(function (TiledOG) {
    var showHello = true;
    function PrepareOject(layer) {
        var props = {};
        if (layer.properties) {
            if (layer.properties instanceof Array) {
                for (var _i = 0, _a = layer.properties; _i < _a.length; _i++) {
                    var p = _a[_i];
                    var val = p.value;
                    if (p.type == "color")
                        val = Tiled.Utils.HexStringToHexInt(val);
                    props[p.name] = val;
                }
            }
            else {
                props = layer.properties;
            }
        }
        // http://doc.mapeditor.org/en/stable/reference/tmx-map-format/#tile-flipping
        if (layer.gid) {
            var gid = layer.gid;
            var vFlip = gid & 0x40000000;
            var hFlip = gid & 0x80000000;
            var dFlip = gid & 0x20000000;
            props["vFlip"] = vFlip;
            props["hFlip"] = hFlip;
            props["dFlip"] = dFlip;
            var realGid = gid & (~(0x40000000 | 0x80000000 | 0x20000000));
            layer.gid = realGid;
        }
        layer.properties = props;
    }
    function ImageFromTileset(tilesets, baseUrl, gid) {
        var tileSet = undefined; //_data.tilesets[0];
        for (var i = 0; i < tilesets.length; i++) {
            if (tilesets[i].firstgid <= gid) {
                tileSet = tilesets[i];
            }
        }
        if (!tileSet) {
            console.log("Image with gid:" + gid + " not found!");
            return null;
        }
        var realGid = gid - tileSet.firstgid;
        var find = tileSet.tiles.filter(function (obj) { return obj.id == realGid; })[0];
        var img = Object.assign({}, find);
        if (!img) {
            console.log("Load res MISSED gid:" + realGid);
            return null;
        }
        img.image = baseUrl + img.image;
        return img;
    }
    function CreateStage(res, loader) {
        var _data = {};
        if (res instanceof PIXI.loaders.Resource) {
            _data = res.data;
        }
        else {
            _data = loader;
        }
        if (!_data || _data.type != "map") {
            //next();
            return undefined;
        }
        if (showHello) {
            console.log("Tiled OG importer!\n eXponenta {rondo.devil[a]gmail.com}");
            showHello = false;
        }
        var useDisplay = TiledOG.Config.usePixiDisplay != undefined && TiledOG.Config.usePixiDisplay && PIXI.display != undefined;
        var Layer = useDisplay ? PIXI.display.Layer : {};
        var Group = useDisplay ? PIXI.display.Group : {};
        var Stage = useDisplay ? PIXI.display.Stage : {};
        var _stage = new TiledOG.TiledContainer(); //useDisplay ?  new Stage() : new TiledContainer();
        var cropName = new RegExp(/^.*[\\\/]/);
        _stage.layerHeight = _data.height;
        _stage.layerWidth = _data.width;
        var baseUrl = "";
        if (res instanceof PIXI.loaders.Resource) {
            _stage.name = res.url.replace(cropName, "").split(".")[0];
            baseUrl = res.url.replace(loader.baseUrl, "");
            baseUrl = baseUrl.match(cropName)[0];
        }
        if (_data.layers) {
            var zOrder = 0; //_data.layers.length;
            if (useDisplay)
                _data.layers = _data.layers.reverse();
            for (var _i = 0, _a = _data.layers; _i < _a.length; _i++) {
                var layer = _a[_i];
                if (layer.type !== "objectgroup" && layer.type !== "imagelayer") {
                    console.warn("OGParser support only OBJECT or IMAGE layes!!");
                    continue;
                }
                PrepareOject(layer);
                var props = layer.properties;
                if (props.ignore || props.ignoreLoad) {
                    console.log("OGParser: ignore loading layer:" + layer.name);
                    continue;
                }
                var pixiLayer = useDisplay
                    ? new Layer(new Group(props.zOrder !== undefined ? props.zOrder : zOrder, true))
                    : new TiledOG.TiledContainer();
                zOrder++;
                pixiLayer.tiledId = layer.id;
                pixiLayer.name = layer.name;
                _stage.layers = {};
                _stage.layers[layer.name] = pixiLayer;
                pixiLayer.visible = layer.visible;
                pixiLayer.position.set(layer.x, layer.y);
                pixiLayer.alpha = layer.opacity || 1;
                TiledOG.ContainerBuilder.ApplyMeta(layer, pixiLayer);
                _stage.addChild(pixiLayer);
                if (layer.type == "imagelayer") {
                    layer.objects = [
                        {
                            img: {
                                image: baseUrl + layer.image
                            },
                            gid: 123456789,
                            name: layer.name,
                            x: layer.x + layer.offsetx,
                            y: layer.y + layer.offsety,
                            fromImageLayer: true,
                            properties: layer.properties
                        }
                    ];
                }
                if (!layer.objects)
                    return undefined;
                //next();
                var localZIndex = 0;
                var _loop_1 = function (layerObj) {
                    PrepareOject(layerObj);
                    if (layerObj.properties.ignore)
                        return "continue";
                    var type = Tiled.Utils.Objectype(layerObj);
                    var pixiObject = null;
                    switch (type) {
                        case Tiled.Utils.TiledObjectType.IMAGE: {
                            if (!layerObj.fromImageLayer) {
                                var img = ImageFromTileset(_data.tilesets, baseUrl, layerObj.gid);
                                if (!img) {
                                    return "continue";
                                }
                                layerObj.img = img;
                            }
                            //Sprite Loader
                            pixiObject = TiledOG.SpriteBuilder.Build(layerObj);
                            var sprite_1 = pixiObject;
                            var cached_1 = undefined;
                            if (loader instanceof PIXI.loaders.Loader) {
                                cached_1 = loader.resources[layerObj.img.image];
                            }
                            else if (res instanceof PIXI.Spritesheet) {
                                cached_1 = res.textures[layerObj.img.image];
                            }
                            if (!cached_1) {
                                if (loader instanceof PIXI.loaders.Loader) {
                                    loader.add(layerObj.img.image, {
                                        parentResource: res
                                    }, function () {
                                        var tex = loader.resources[layerObj.img.image].texture;
                                        sprite_1.texture = tex;
                                        if (layerObj.fromImageLayer) {
                                            sprite_1.scale.set(1);
                                        }
                                    });
                                }
                                else {
                                    return "continue";
                                }
                            }
                            else {
                                if (cached_1 instanceof PIXI.loaders.Resource) {
                                    if (!cached_1.isComplete) {
                                        cached_1.onAfterMiddleware.once(function () {
                                            sprite_1.texture = cached_1.texture;
                                            if (layerObj.fromImageLayer) {
                                                sprite_1.scale.set(1);
                                            }
                                        });
                                    }
                                    else {
                                        sprite_1.texture = cached_1.texture;
                                        if (layerObj.fromImageLayer) {
                                            sprite_1.scale.set(1);
                                        }
                                    }
                                }
                                else if (cached_1) {
                                    sprite_1.texture = cached_1;
                                    //sprite.height = (cached as any).height;
                                    //sprite.width = (cached as any).width;
                                    if (layerObj.fromImageLayer) {
                                        sprite_1.scale.set(1);
                                    }
                                }
                            }
                            break;
                        }
                        // TextLoader
                        case Tiled.Utils.TiledObjectType.TEXT: {
                            pixiObject = TiledOG.TextBuilder.Build(layerObj);
                            break;
                        }
                        default: {
                            pixiObject = TiledOG.ContainerBuilder.Build(layerObj);
                        }
                    }
                    if (TiledOG.Config.usePixiDisplay) {
                        pixiObject.parentGroup = pixiLayer.group;
                        _stage.addChildAt(pixiObject, localZIndex);
                    }
                    else {
                        pixiLayer.addChildAt(pixiObject, localZIndex);
                    }
                    localZIndex++;
                };
                for (var _b = 0, _c = layer.objects; _b < _c.length; _b++) {
                    var layerObj = _c[_b];
                    _loop_1(layerObj);
                }
            }
        }
        return _stage;
    }
    TiledOG.CreateStage = CreateStage;
    var Parser = /** @class */ (function () {
        function Parser() {
        }
        Parser.prototype.consructor = function () { };
        Parser.prototype.Parse = function (res, next) {
            var stage = CreateStage(res, this);
            res.stage = stage;
            next();
        };
        return Parser;
    }());
    TiledOG.Parser = Parser;
})(TiledOG || (TiledOG = {}));
var TiledOG;
(function (TiledOG) {
    var Primitives;
    (function (Primitives) {
        var TiledRect = /** @class */ (function (_super) {
            __extends(TiledRect, _super);
            function TiledRect() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.name = "";
                _this.types = [];
                _this.visible = true;
                return _this;
            }
            return TiledRect;
        }(PIXI.Rectangle));
        Primitives.TiledRect = TiledRect;
        var TiledPoint = /** @class */ (function (_super) {
            __extends(TiledPoint, _super);
            function TiledPoint(x, y) {
                var _this = _super.call(this, x, y) || this;
                _this.name = "";
                _this.types = [];
                _this.visible = true;
                return _this;
            }
            return TiledPoint;
        }(PIXI.Point));
        Primitives.TiledPoint = TiledPoint;
        var TiledPolygon = /** @class */ (function (_super) {
            __extends(TiledPolygon, _super);
            function TiledPolygon(points) {
                var _this = _super.call(this, points) || this;
                _this.name = "";
                _this.types = [];
                _this.visible = true;
                _this._x = 0;
                _this._y = 0;
                return _this;
            }
            Object.defineProperty(TiledPolygon.prototype, "x", {
                get: function () {
                    return this._x;
                },
                set: function (sX) {
                    var delta = sX - this._x;
                    this._x = sX;
                    for (var xIndex = 0; xIndex < this.points.length; xIndex += 2) {
                        this.points[xIndex] += delta;
                    }
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TiledPolygon.prototype, "y", {
                get: function () {
                    return this._y;
                },
                set: function (sY) {
                    var delta = sY - this._y;
                    this._y = sY;
                    for (var yIndex = 1; yIndex < this.points.length; yIndex += 2) {
                        this.points[yIndex] += delta;
                    }
                },
                enumerable: true,
                configurable: true
            });
            TiledPolygon.prototype.getBounds = function () {
                var rect = new PIXI.Rectangle();
                var maxX = this._x;
                var maxY = this._y;
                for (var index = 0; index < this.points.length; index += 2) {
                    var px = this.points[index];
                    var py = this.points[index + 1];
                    rect.x = px < rect.x ? px : rect.x;
                    rect.y = py < rect.y ? py : rect.y;
                    maxX = px > maxX ? px : maxX;
                    maxY = py > maxY ? py : maxY;
                }
                rect.width = maxX - rect.x;
                rect.height = maxY - rect.y;
                return rect;
            };
            Object.defineProperty(TiledPolygon.prototype, "width", {
                get: function () {
                    return this.getBounds().width;
                },
                set: function (w) {
                    var factor = w / this.width;
                    for (var xIndex = 0; xIndex < this.points.length; xIndex += 2) {
                        var delta = (this.points[xIndex] - this._x) * factor;
                        this.points[xIndex] = this._x + delta;
                    }
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(TiledPolygon.prototype, "height", {
                get: function () {
                    return this.getBounds().height;
                },
                set: function (h) {
                    var factor = h / this.height;
                    for (var yIndex = 1; yIndex < this.points.length; yIndex += 2) {
                        var delta = (this.points[yIndex] - this._y) * factor;
                        this.points[yIndex] = this._y + delta;
                        ;
                    }
                },
                enumerable: true,
                configurable: true
            });
            return TiledPolygon;
        }(PIXI.Polygon));
        Primitives.TiledPolygon = TiledPolygon;
        var TiledPolypine = /** @class */ (function () {
            function TiledPolypine(points) {
                this.name = "";
                this.types = [];
                this.visible = true;
                this.points = [];
                this.points = points.slice();
            }
            return TiledPolypine;
        }());
        Primitives.TiledPolypine = TiledPolypine;
        var TiledEllipse = /** @class */ (function (_super) {
            __extends(TiledEllipse, _super);
            function TiledEllipse(x, y, hw, hh) {
                var _this = _super.call(this, x, y, hw, hh) || this;
                _this.name = "";
                _this.types = [];
                _this.visible = true;
                return _this;
            }
            return TiledEllipse;
        }(PIXI.Ellipse));
        Primitives.TiledEllipse = TiledEllipse;
        function BuildPrimitive(meta) {
            if (!meta)
                return;
            var prim = undefined;
            var type = Tiled.Utils.Objectype(meta);
            meta.x = meta.x || 0;
            meta.y = meta.y || 0;
            switch (type) {
                case Tiled.Utils.TiledObjectType.ELLIPSE: {
                    prim = new TiledEllipse(meta.x + 0.5 * meta.width, meta.y + 0.5 * meta.height, meta.width * 0.5, meta.height * 0.5);
                    break;
                }
                case Tiled.Utils.TiledObjectType.POLYGON: {
                    var points = meta.polygon;
                    var poses = points.map(function (p) {
                        return new PIXI.Point(p.x + meta.x, p.y + meta.y);
                    });
                    prim = new TiledPolygon(poses);
                    break;
                }
                case Tiled.Utils.TiledObjectType.POLYLINE: {
                    var points = meta.polygon;
                    var poses = points.map(function (p) {
                        return new PIXI.Point(p.x + meta.x, p.y + meta.y);
                    });
                    prim = new TiledPolypine(poses);
                    break;
                }
                default:
                    prim = new TiledRect(meta.x, meta.y, meta.width, meta.height);
            }
            prim.types = meta.type ? meta.type.split(":") : [];
            prim.visible = meta.visible;
            prim.name = meta.name;
            return prim;
        }
        Primitives.BuildPrimitive = BuildPrimitive;
    })(Primitives = TiledOG.Primitives || (TiledOG.Primitives = {}));
})(TiledOG || (TiledOG = {}));
var Tiled;
(function (Tiled) {
    var Utils;
    (function (Utils) {
        function HexStringToHexInt(value) {
            if (!value)
                return 0;
            if (typeof value == "number")
                return value;
            value = value.length > 7 ? value.substr(3, 6) : value.substr(1, 6);
            try {
                return parseInt(value, 16);
            }
            catch (e) {
                console.warn("Color parse error:", e.message);
                return 0;
            }
        }
        Utils.HexStringToHexInt = HexStringToHexInt;
        function HexStringToAlpha(value) {
            if (!value)
                return 1;
            if (typeof value == "number")
                return value;
            if (value.length <= 7)
                return 1;
            try {
                return parseInt(value.substr(1, 2), 16) / 255.0;
            }
            catch (e) {
                console.warn("Alpha parse error:", e.message);
                return 1;
            }
        }
        Utils.HexStringToAlpha = HexStringToAlpha;
        var TiledObjectType;
        (function (TiledObjectType) {
            TiledObjectType[TiledObjectType["DEFAULT"] = 0] = "DEFAULT";
            TiledObjectType[TiledObjectType["POINT"] = 1] = "POINT";
            TiledObjectType[TiledObjectType["POLYGON"] = 2] = "POLYGON";
            TiledObjectType[TiledObjectType["POLYLINE"] = 3] = "POLYLINE";
            TiledObjectType[TiledObjectType["ELLIPSE"] = 4] = "ELLIPSE";
            TiledObjectType[TiledObjectType["TEXT"] = 5] = "TEXT";
            TiledObjectType[TiledObjectType["IMAGE"] = 6] = "IMAGE";
        })(TiledObjectType = Utils.TiledObjectType || (Utils.TiledObjectType = {}));
        // https://doc.mapeditor.org/en/stable/reference/json-map-format/
        function Objectype(meta) {
            if (meta.properties && meta.properties.container)
                return TiledObjectType.DEFAULT;
            if (meta.gid || meta.image)
                return TiledObjectType.IMAGE;
            if (meta.text != undefined)
                return TiledObjectType.TEXT;
            if (meta.point)
                return TiledObjectType.POINT;
            if (meta.polygon)
                return TiledObjectType.POLYGON;
            if (meta.polyline)
                return TiledObjectType.POLYLINE;
            if (meta.ellipse)
                return TiledObjectType.ELLIPSE;
            return TiledObjectType.DEFAULT;
        }
        Utils.Objectype = Objectype;
    })(Utils = Tiled.Utils || (Tiled.Utils = {}));
})(Tiled || (Tiled = {}));
var TiledOG;
(function (TiledOG) {
    TiledOG.Config = {
        defSpriteAnchor: new PIXI.Point(0, 1),
        debugContainers: false,
        usePixiDisplay: false,
        roundFontAlpha: false
    };
    TiledOG.Builders = [
        TiledOG.ContainerBuilder.Build,
        TiledOG.SpriteBuilder.Build,
        TiledOG.TextBuilder.Build
    ];
    function InjectToPixi(props) {
        if (props === void 0) { props = undefined; }
        if (props) {
            TiledOG.Config.defSpriteAnchor = props.defSpriteAnchor || TiledOG.Config.defSpriteAnchor;
            TiledOG.Config.debugContainers = props.debugContainers != undefined
                ? props.debugContainers
                : TiledOG.Config.debugContainers;
            TiledOG.Config.usePixiDisplay = props.usePixiDisplay != undefined
                ? props.usePixiDisplay
                : TiledOG.Config.usePixiDisplay;
            TiledOG.Config.roundFontAlpha = props.roundFontAlpha != undefined
                ? props.roundFontAlpha
                : TiledOG.Config.roundFontAlpha;
        }
        var parser = new TiledOG.Parser();
        PIXI.loaders.Loader.addPixiMiddleware(function () { return parser.Parse; });
        console.log("Now you use Tiled!");
    }
    TiledOG.InjectToPixi = InjectToPixi;
})(TiledOG || (TiledOG = {}));
//TiledOG.InjectToPixi();
PIXI.Container.prototype.getChildByPath = function (path) {
    if (!(this instanceof PIXI.Container) || this.children.length == 0)
        return undefined;
    var result = this;
    var split = path.split("/");
    var isIndex = new RegExp("(?:\{{0})-?\d+(?=\})");
    for (var _i = 0, split_1 = split; _i < split_1.length; _i++) {
        var p = split_1[_i];
        if (result == undefined || !(result instanceof PIXI.Container)) {
            result = undefined;
            break;
        }
        if (p.trim().length == 0)
            continue;
        // find by index
        var mathes = p.match(isIndex);
        if (mathes) {
            var index = parseInt(mathes[0]);
            if (index < 0) {
                index += result.children.length;
            }
            if (index >= result.children.length) {
                result = undefined;
            }
            else {
                result = result.children[index];
            }
            continue;
        }
        //default by name
        result = result.getChildByName(p);
    }
    return result;
};
PIXI.DisplayObject.prototype.replaceWithTransform = function (from) {
    from.updateTransform();
    if (from.parent)
        from.parent.addChildAt(this, from.parent.getChildIndex(from));
    this.position.copy(from.position);
    this.scale.copy(from.scale);
    this.rotation = from.rotation;
    this.updateTransform();
};
var InputHandler;
(function (InputHandler) {
    InputHandler.IsKeyDown = [];
    var latestDom;
    function upHandler(e) {
        e.preventDefault();
        InputHandler.IsKeyDown[e.keyCode] = false;
    }
    function downHandler(e) {
        e.preventDefault();
        InputHandler.IsKeyDown[e.keyCode] = true;
    }
    function focusOut(e) {
        InputHandler.IsKeyDown = [];
    }
    function BindKeyHandler(dom) {
        latestDom = dom;
        dom.addEventListener("keydown", downHandler);
        dom.addEventListener("keyup", upHandler);
        dom.addEventListener("blur", focusOut);
    }
    InputHandler.BindKeyHandler = BindKeyHandler;
    function ReleaseKeyHandler() {
        if (latestDom) {
            latestDom.removeEventListener("keydown", downHandler);
            latestDom.removeEventListener("keyup", upHandler);
            latestDom.removeEventListener("blur", focusOut);
            latestDom = undefined;
        }
        InputHandler.IsKeyDown = [];
    }
    InputHandler.ReleaseKeyHandler = ReleaseKeyHandler;
})(InputHandler || (InputHandler = {}));
PIXI.loaders.Loader.prototype.filter = function (func) {
    if (!func)
        return [];
    var ress = this.resources;
    var ret = [];
    var keys = Object.keys(ress);
    keys.forEach(function (k) {
        if (func(ress[k])) {
            ret.push(ress[k]);
        }
    });
    return ret;
};
//# sourceMappingURL=game.js.map