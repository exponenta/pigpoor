namespace PigPoorGame.Objects {

	export class CoinsObject extends PIXI.Container {

		anim: PIXI.extras.AnimatedSprite;
		layer: ILayer;
        shitTextPool: Array<string> = [];
        speeds: PIXI.Point = new PIXI.Point(0,0);
        
        isShowed: boolean  = false;

		constructor(layer: ILayer) {
			super();

			this.layer = layer;
			const coins = layer.app.loader.resources["anims"].spritesheet!.animations["coin"]!;
			this.anim = new PIXI.extras.AnimatedSprite(coins);
			this.anim.animationSpeed  = 0.2;
			this.anim.anchor.set(0.5);
			this.addChild(this.anim);
			//this.setText("NDFL 20");
		}

        update(dt: number) {

            this.speeds.y += dt * 100 / 60;

            this.position.x += dt * this.speeds.x  / 60;
            this.position.y += dt * this.speeds.y  / 60;
            
        }
	}
}
