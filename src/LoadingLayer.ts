namespace PigPoorGame {
	export class LoadingLayer implements ILayer {
		stage?: TiledOG.TiledContainer;
		app: App;
		loader?: PIXI.loaders.Loader;

		constructor(app: App) {
			this.app = app;
		}

		addToLoader(loader: PIXI.loaders.Loader, callback?: () => void): PIXI.loaders.Loader {
			this.loader = loader;

			loader.add("boy_run_animation", "./maps/preload.json");
			loader.add("loading", "./maps/loading_layer.json", () => {
				if (callback) callback();
			});

			return loader;
		}

		init(): void {
			if (!this.loader) return;
			this.stage = this.loader.resources["loading"].stage;

			const sprites = this.loader.resources["boy_run_animation"].spritesheet;

			if (sprites && this.stage) {
				const anim = new PIXI.extras.AnimatedSprite(sprites.animations["boy_run"]);
				anim.loop = true;
				anim.animationSpeed = 0.2;
				anim.play();

				const loading_anim = this.stage.getChildByPath<PIXI.Sprite>("LoadingScreen/Loading_Anim");

				if (loading_anim) {
					anim.replaceWithTransform(loading_anim);
					anim.anchor.copy(loading_anim.anchor);
					loading_anim.destroy();
				}
			}

			this.app.activateLayer(this);
		}

		showed(): void {
			this.loadNext();
		}

		hidden(): void {}
		fadeProgress(p: number, showing: boolean): void {}
        
		update(delta: number): void {

		}

		loadNext() {
			if (!this.loader) return;

			const intr = this.app.soundManager.Play("intro", true,0.5);
	
			let nextLayer: any = this.app.layers["login"];
			/*
			if (PigPoorGame.DEBUG) {
				this.app.userConfig.isMen = Math.random() > 0.5;
				this.app.userConfig.name = "TEST";
				this.app.userConfig.uuid = PIXI.utils.uid() + "";

				nextLayer = this.app.layers["game"];
			} else {
				nextLayer = this.app.layers["login"];
			}*/

			const parallax = this.app.layers["parallax"];
			parallax.addToLoader(this.loader);
			this.app.layers["game"].addToLoader(this.loader);
			this.app.layers["login"].addToLoader(this.loader);
			this.app.layers["result"].addToLoader(this.loader);
			
			this.loader!.add("anims","./maps/animations.json");
			this.loader.load(() => {
				
				parallax.init();
				nextLayer.init();
				this.app.activateLayer(nextLayer);
			});
		}
	}
}
