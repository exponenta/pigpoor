
namespace PigPoorGame {
	export interface NetConfig {
		ajaxUrl?: string;
		firebase?: firebase.firestore.Settings;
	}

	export class NetManager {
		config?: NetConfig;
		collRef?: firebase.firestore.CollectionReference;

		constructor(config: NetConfig | undefined = undefined) {
			this.config = config;
			if (this.config && this.config.firebase) {
				firebase.initializeApp(this.config.firebase);
				const firebaseDB = firebase.firestore();
				firebaseDB.settings({
					timestampsInSnapshots: true
				});
				this.collRef = firebaseDB.collection("leaderboard");
			}
		}

		sendScore(
			prof: UserProfile | undefined,
			callback: (profs: UserProfile[] | undefined) => void,
			count: number = 10
		) {
			if (!this.config || (!this.config.ajaxUrl && !this.config.firebase)) return;

			if (this.config.ajaxUrl) {
				this._sendXHR(prof, callback);
				return;
			}

			if (this.collRef) {
				this.collRef
					.orderBy("score", "desc")
					.limit(count)
					.get()
					.then(data => {
						var results: UserProfile[] = new Array<UserProfile>();
						if (!data.empty) {
							data.docs.forEach(doc => {
								results.push(doc.data() as UserProfile);
							});
						}
						callback(results);
					});

				if (prof) {
					this.collRef.add( {
						name: prof.name,
						gender: prof.gender,
						score: prof.score,
						roundDiration: prof.roundDiration
					} as UserProfile);
				}
			}
		}

		getScores(callback: (profs: UserProfile[] | undefined) => void, count: number = 10) {
			this.sendScore(undefined, callback, count);
		}

		private _sendXHR(prof: UserProfile | undefined, callback: (profs: UserProfile[] | undefined) => void) {
			const req = new XMLHttpRequest();
			req.setRequestHeader("Content-Type", "application/json");
			var data = undefined;
			if (prof) {
				data = new FormData();
				data.append("name", prof.name);
				data.append("isMan", "" + !!prof.isMen);
				data.append("score", "" + prof.score);
				req.open("POST", this.config!.ajaxUrl!, true);
			} else {
				req.open("GET", this.config!.ajaxUrl!, true);
			}

			req.onload = ev => {
				if (req.status == 200) {
					callback(JSON.parse(req.responseText));
					return;
				}
				callback(undefined);
			};

			req.send(data);
		}
	}
}
